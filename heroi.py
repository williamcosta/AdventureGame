'''
Adventure Game
Copyright (c) 2015-2021, William Alcântara Costa
All rights reserved.
- This code is licensed under the BSD 3-Clause License - check it on LICENSE.MD

---

Adventure Game (Jogo de aventura)
Copyright (c) 2015-2021, William Alcântara Costa
Todos os direitos reservados.
- Este código está licensiado sob a Licença de três cláusulas BSD - leia mais em LICENSE.MD

'''

import random, base64 # invoca o random para selecionar numeros

nome = "heroibasico"
HP = 50
maxHP = 50
armadura = 1 # reduz um pouco o dano causado
atkMin = 10
atkMax = 13
velocidade = 6 # aumenta as chances de esquivar de um ataque
forca = 2 # aumenta as chances de criticos
gold = 0 # dinheiro
exp = 0 # experiência
expNext = 40 # experiência necessária para subir level
level = 1 # level
passosRestantesRepel = 0 # passos restantes pra terminar o efeito do repelente
passosRestantesVeneno = 0 # passos/ações restantes para acabar com o veneno

# Flags
isDanoCrit = 0 # Mensagem se deu dano crítico
achouPortao = 0 # Se ja passou alguma vez sob o portão
achouLoja = 0 # Se ja passou alguma vez sob a loja
achouChave = 0 # Se ja passou alguma vez pela chave e não pegou
achouPousada = 0 # Se ja passou alguma vez pela pousada
assistiuCutscene = 0 # Se ja assistiu a cutscene inicial

# Itens
item1Quant = 0
item1Tipo = 0
item2Quant = 0
item2Tipo = 0
item3Quant = 0
item3Tipo = 0
item4Quant = 0
item4Tipo = 0

#posições X e Y
posX = 0
posY = 0

#posições do mapa e tempo do save
chaveX = 0
chaveY = 0
portaoX = 0
portaoY = 0
NPCX = 0
NPCY = 0
fadX = 0
fadY = 0
pouX = 0
pouY = 0
tDias = 0
tHoras = 0
tMinutos = 0

def setarPosicaoInicial(limiteX, limiteY):
	global posX, posY
	posX = random.randint(1, limiteX)
	posY = random.randint(1, limiteY)

def atacar(danoExtra,forcaExtra):
	global atkMin, atkMax, forca, isDanoCrit # invoca as variaveis globais

	# Verifica se a força for maior que 7 (50% críticos) reduz para 7 (33% críticos)
	tmpForca = forca+forcaExtra
	if(tmpForca > 7):
		tmpForca = 7

	isCritico = random.randint(1, 10-tmpForca) # testa a chance de um ataque critico
	ataque = random.randint(atkMin, atkMax+danoExtra) # gera o valor de um ataque
	if(isCritico != 1): # se o ataque NÃO for critico
		isDanoCrit = 0
		return ataque
	else: # se for crítico
		isDanoCrit = 1
		return ataque * 2 # duplica o valor

def receberDano(quantidadeDano, recebeuCritico, recebeuVeneno, armaduraExtra):
	global HP, armadura, velocidade, passosRestantesVeneno
	chanceEsquiva = random.randint(1, 15-velocidade)
	if(chanceEsquiva != 1): # se NÃO esquivar o ataque
		quantDano = quantidadeDano - (int((armadura + armaduraExtra) * 0.65))
		if(quantDano < 0):
			quantDano = 0
		HP = HP - quantDano
		if(recebeuCritico == 0):
			print(str(quantDano) + " recebidos de dano!")
		else:
			print(str(quantDano) + " recebidos de dano CRÍTICO!")
		if(recebeuVeneno == 1):
			passosRestantesVeneno = 12
			print("Você foi envenenado!")
	else:
		print("Se esquivou do golpe!")
	isDanoCrit = 0
		
def subirNivel():
	global exp, expNext, level, HP, maxHP, armadura, atkMin, atkMax, velocidade, forca
	if(exp >= expNext):
		HP = HP + 5
		maxHP = maxHP + 5
		armadura = armadura + 1
		atkMin = atkMin + 3
		atkMax = atkMax + 3
		if(velocidade < 11):
			velocidade = velocidade + 1
		if(forca < 7):
			forca = forca + 1
		expNext = expNext * 2
		level = level + 1
		print("Você subiu de nível!")

def caminhar(direcao, limite):
	global posX, posY, passosRestantesRepel
	if(direcao == 1): #caminhar norte
		if(posY != 1):
			posY = posY - 1
			if(passosRestantesRepel > 0):
				passosRestantesRepel = passosRestantesRepel -1
				if(passosRestantesRepel == 0):
					print("OK, o efeito de repelente acabou!")
				else:
					print("OK")
			return 1
		else:
			print("Impossível caminhar norte!")
			return 0
	elif(direcao == 2): # caminhar sul
		if(posY != limite):
			posY = posY + 1
			if(passosRestantesRepel > 0):
				passosRestantesRepel = passosRestantesRepel -1
				if(passosRestantesRepel == 0):
					print("OK, o efeito de repelente acabou!")
				else:
					print("OK")
			return 1
		else:
			print("Impossível caminhar sul!")
			return 0
	elif(direcao == 3): # caminhar leste
		if(posX != limite):
			posX = posX + 1
			if(passosRestantesRepel > 0):
				passosRestantesRepel = passosRestantesRepel -1
				if(passosRestantesRepel == 0):
					print("OK, o efeito de repelente acabou!")
				else:
					print("OK")
			return 1
		else:
			print("Impossível caminhar leste!")
			return 0
	else: # caminhar oeste
		if(posX != 1):
			posX = posX - 1
			if(passosRestantesRepel > 0):
				passosRestantesRepel = passosRestantesRepel -1
				if(passosRestantesRepel == 0):
					print("OK, o efeito de repelente acabou!")
				else:
					print("OK")
			return 1
		else:
			print("Impossível caminhar oeste!")
			return 0

def verStatus(aumentoAtk,aumentoForca,aumentoArmadura, horario):
	global nome, HP, maxHP, armadura, atkMin, atkMax, velocidade, forca, gold, exp, expNext, level, pocoes, possuiChave
	print(nome)
	print("HP: " + str(HP) + "/" + str(maxHP) + " | Level: " + str(level))

	if(aumentoAtk > 0):
		print("Ataque: " + str(atkMin) + "(min) / " + str(atkMax) + "(max) + " + str(aumentoAtk))
	else:
		print("Ataque: " + str(atkMin) + "(min) / " + str(atkMax) + "(max)")

	print("Dinheiro: " + str(gold) + "$")
	print("Experiência: " + str(exp) + "/" + str(expNext))

	if(aumentoForca > 0):
		print("Força: " + str(forca) + " + " + str(aumentoForca))
	else:
		print("Força: " + str(forca))

	print("Velocidade: " + str(velocidade))
	
	if(aumentoArmadura > 0):
		print("Armadura: " + str(armadura) + " + " + str(aumentoArmadura))
	else:
		print("Armadura: " + str(armadura))

	if(passosRestantesRepel > 2):
		print("Repelente ATIVO por mais " + str(passosRestantesRepel) + " passos")
	elif(passosRestantesRepel == 1):
		print("Repelente ATIVO por mais " + str(passosRestantesRepel) + " passo")

	if(passosRestantesVeneno > 2):
		print("Veneno ativo por cerca de mais " +str(passosRestantesVeneno) + " passos")
	elif(passosRestantesVeneno == 1):
		print("Veneno ativo por cerca de mais " +str(passosRestantesVeneno) + " passo")
	
	print("---\n" + horario + "\n---\nInventário: ")

def beberPocao(tipoPocao, quantPoc, quantCura):
	global HP, maxHP
	if(quantPoc > 0):
		if(HP < maxHP):
			HP = HP + quantCura
			quantoCurado = str(quantCura) + " HP" # armazena a quantidade curada
			if(HP > maxHP): # Se o HP ficar maior que o máximo
				temp = HP - maxHP # salva temporáriamente a diferença
				HP = maxHP # Seta o HP para o máximo
				quantoCurado = str(quantCura - temp) + " HP" # altera a mensagem para a diferença
			if(tipoPocao == 0):
				print("Você bebeu uma poção que curou " + quantoCurado + "!")
			else:
				print("Você bebeu uma super poção que curou " + quantoCurado + "!")
			return 1
		else:
			print("Seu HP está cheio! Não é necessario beter uma poção")
			return 0
	else:
		if(tipoPocao == 0):
			print("Você não possui nenhuma poção!")
		else:
			print("Você não possui nenhuma super poção!")
		return 0

def salvarJogo(chaveX, chaveY, portaoX, portaoY, NPCX, NPCY, MNX, MNY, fadX, fadY, pouX, pouY, item1Tipo, item1Quant, item2Tipo, item2Quant, item3Tipo, item3Quant, item4Tipo, item4Quant, dias, horas, minutos):
	global nome, HP, maxHP, armadura, atkMin, atkMax, velocidade, forca, gold, exp, expNext, level, posX, posY, achouPortao, achouLoja, achouChave, achouPousada, passosRestantesRepel
	
	# Guarda as informações sobre o herói | Array de 0 - 14
	infoHeroi = str(HP) + "," + str(maxHP) + "," + str(armadura) + "," + str(atkMin) + "," + str(atkMax) + "," + str(velocidade) + "," + str(forca) + "," + str(gold) + "," + str(exp) + "," + str(expNext) + "," + str(level) + "," + str(posX) + "," + str(posY) + "," + str(passosRestantesRepel) + "," + str(passosRestantesVeneno)
	
	# Guarda as informações do mapa | Array de 15 - 30
	infoMapa = str(chaveX) + "," + str(chaveY) + "," + str(portaoX) + "," + str(portaoY) + "," + str(NPCX) + "," + str(NPCY) + "," + str(MNX) + "," + str(MNY) + "," + str(fadX) + "," + str(fadY) + "," + str(pouX) + "," + str(pouY) + "," + str(achouPortao) + "," + str(achouLoja) + "," + str(achouChave) + "," + str(achouPousada)
	
	# Guarda as informações dos itens | Array de 31 - 38
	infoItens = str(item1Tipo) + "," + str(item1Quant) + "," + str(item2Tipo) + "," + str(item2Quant) + "," + str(item3Tipo) + "," + str(item3Quant) + "," + str(item4Tipo) + "," + str(item4Quant)
	
	# Guarda as informações do tempo | Array de 39 - 41
	infoTempo = str(dias) + "," + str(horas) + "," + str(minutos)
	
	
	file = open("saves/" + nome + '.adv','w') # abre o arquivo para salvar

	# Gera o texto a ser codificado
	texto = infoHeroi + "," + infoMapa + "," + infoItens + "," + infoTempo
	texto = base64.b64encode(texto.encode('ascii'))
	
	texto = texto.decode("utf-8")
	
	# Salva os dados no arquivo
	file.write(texto)
	
	file.close() # fecha o arquivo
	print("Jogo salvo com sucesso!")

def abrirJogo():
	global nome, HP, maxHP, armadura, atkMin, atkMax, velocidade, forca, gold, exp, expNext, level, pocoes, possuiChave, posX, posY, passosRestantesRepel, passosRestantesVeneno, chaveX, chaveY, portaoX, portaoY, NPCX, NPCY, MNX, MNY, fadX, fadY, pouX, pouY, item1Tipo, item1Quant, item2Tipo, item2Quant, item3Tipo, item3Quant, item4Tipo, item4Quant, achouPortao, achouLoja, achouChave, achouPousada, tDias, tHoras, tMinutos
	print("Tentando abrir jogo...")
	try:
		file = open("saves/" + nome + ".adv",'r') # abre o arquivo para leitura
		for line in file:
			texto = base64.b64decode(str(line))
		texto = str(texto, 'utf-8')
		texto = texto.split(',')

		contAbrir = 0
		for x in range(0,42):
			if(contAbrir == 0):
				HP = int(texto[x])
			elif(contAbrir == 1):
				maxHP = int(texto[x])
			elif(contAbrir == 2):
				armadura = int(texto[x])
			elif(contAbrir == 3):
				atkMin = int(texto[x])
			elif(contAbrir == 4):
				atkMax = int(texto[x])
			elif(contAbrir == 5):
				velocidade = int(texto[x])
			elif(contAbrir == 6):
				forca = int(texto[x])
			elif(contAbrir == 7):
				gold = int(texto[x])
			elif(contAbrir == 8):
				exp = int(texto[x])
			elif(contAbrir == 9):
				expNext = int(texto[x])
			elif(contAbrir == 10):
				level = int(texto[x])
			elif(contAbrir == 11):
				posX = int(texto[x])
			elif(contAbrir == 12):
				posY = int(texto[x])
			elif(contAbrir == 13):
				passosRestantesRepel = int(texto[x])
			elif(contAbrir == 14):
				passosRestantesVeneno = int(texto[x]) # FIM DAS INFORMAÇÕES HEROI
			elif(contAbrir == 15):
				chaveX = int(texto[x])
			elif(contAbrir == 16):
				chaveY = int(texto[x])
			elif(contAbrir == 17):
				portaoX = int(texto[x])
			elif(contAbrir == 18):
				portaoY = int(texto[x])
			elif(contAbrir == 19):
				NPCX = int(texto[x])
			elif(contAbrir == 20):
				NPCY = int(texto[x])
			elif(contAbrir == 21):
				MNX = int(texto[x])
			elif(contAbrir == 22):
				MNY = int(texto[x])
			elif(contAbrir == 23):
				fadX = int(texto[x])
			elif(contAbrir == 24):
				fadY = int(texto[x])
			elif(contAbrir == 25):
				pouX = int(texto[x])
			elif(contAbrir == 26):
				pouY = int(texto[x])
			elif(contAbrir == 27):
				achouPortao = int(texto[x])
			elif(contAbrir == 28):
				achouLoja = int(texto[x])
			elif(contAbrir == 29):
				achouChave = int(texto[x])
			elif(contAbrir == 30):
				achouPousada == int(texto[x]) # FIM INFORMAÇÕES MAPA
			elif(contAbrir == 31):
				item1Tipo = int(texto[x])
			elif(contAbrir == 32):
				item1Quant = int(texto[x])
			elif(contAbrir == 33):
				item2Tipo = int(texto[x])
			elif(contAbrir == 34):
				item2Quant = int(texto[x])
			elif(contAbrir == 35):
				item3Tipo = int(texto[x])
			elif(contAbrir == 36):
				item3Quant = int(texto[x])
			elif(contAbrir == 37):
				item4Tipo = int(texto[x])
			elif(contAbrir == 38):
				item4Quant = int(texto[x]) # FIM INFORMAÇÕES ITENS
			elif(contAbrir == 39):
				tDias = int(texto[x])
			elif(contAbrir == 40):
				tHoras = int(texto[x])
			elif(contAbrir == 41):
				tMinutos = int(texto[x]) # FIM INFORMAÇÕES TEMPO
			else:
				print("Dunno")
			contAbrir = contAbrir + 1
		#fim for
		file.close()
		print("\nJogo carregado com sucesso!\n---\n")
	except FileNotFoundError:
		print("Não há nenhum jogo salvo com o nome de herói atual!")

def verificarPossuiJogoSalvo(nome):
	try:
		file = open("saves/" + nome + ".adv",'r')
		file.close()
		return 1
	except:
		return 0

def fugirBatalha():
	global velocidade
	if(velocidade < 8):
		return random.randint(1, 10-velocidade)
	else:
		return random.randint(1, 10-8)

def chanceBatalha():
	return random.randint(1, 6)

def verificaVeneno():
	global HP, passosRestantesVeneno
	if(passosRestantesVeneno > 0 and HP > 1):
		HP = HP - 1
		passosRestantesVeneno = passosRestantesVeneno - random.randint(1,3)
		print("Você sofreu dano por estar envenenado!")
	elif(passosRestantesVeneno > 0 and HP == 1):
		passosRestantesVeneno = passosRestantesVeneno - random.randint(1,3)
		print("Você se sente fraco devido ao veneno!")
