'''
Adventure Game
Copyright (c) 2015-2021, William Alcântara Costa
All rights reserved.
- This code is licensed under the BSD 3-Clause License - check it on LICENSE.MD

---

Adventure Game (Jogo de aventura)
Copyright (c) 2015-2021, William Alcântara Costa
Todos os direitos reservados.
- Este código está licensiado sob a Licença de Três Cláusulas BSD - leia mais em LICENSE.MD

'''

import random

#------------------------------------------
#	   Carregar o jogo
#------------------------------------------
print ("Carregando o jogo...\n")

import mundo
mundo.sortearPosicaoChave()
mundo.sortearPosicaoPortao()
mundo.sortearPosicaoNPC()
print ("Mundo carregado!")

import item
item.setarItem(0, 1, 5)
print ("Itens carregados!")

import heroi
heroi.setarPosicaoInicial(mundo.limiteX, mundo.limiteY)
print("Herói carregado!")

import inimigo
print ("Inimigos carregados!")

import os
if not os.path.exists("saves"):
	print("Pasta saves não encontrada, ela será criada.")
	os.makedirs("saves")
	print("Pasta para salvar saves do jogo criada!")
else:
	print("Pasta saves encontrada!")

print("\n---\n")

#------------------------------------------
#	   Métodos do jogo
#------------------------------------------

def carregarJogo():
	heroi.abrirJogo()
	mundo.abrirPosicoes(heroi.chaveX, heroi.chaveY, heroi.portaoX, heroi.portaoY, heroi.NPCX, heroi.NPCY, heroi.MNX, heroi.MNY, heroi.fadX, heroi.fadY, heroi.pouX, heroi.pouY)
	mundo.abrirHorario(heroi.tDias, heroi.tHoras, heroi.tMinutos)
	item.setarItem(0, heroi.item1Tipo, heroi.item1Quant)
	item.setarItem(1, heroi.item2Tipo, heroi.item2Quant)
	item.setarItem(2, heroi.item3Tipo, heroi.item3Quant)
	item.setarItem(3, heroi.item4Tipo, heroi.item4Quant)
	
#---------------------
#
# VENDEDOR DA LOJA
#
#--------------------

def comprarItemUnico(valor, idItem, nomeItem, generoFrase):
	if(heroi.gold >= valor):
		if(item.retornaPrimeiroSlotVazio() != -1):
			if(generoFrase == 0):
				print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado!\n| Aqui está seu " + nomeItem))
			else:
				print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado!\n| Aqui está sua " + nomeItem))
			item.setarItem(item.retornaPrimeiroSlotVazio(), idItem, 1)
			heroi.gold = heroi.gold - valor
			pausa()
		else:
			print(mundo.recebeFala(0,0,"Gaben Ewell","Lamento!\n| Você não tem espaço no intenvário!"))
			pausa()
	else:
		print(mundo.recebeFala(0,0,"Gaben Ewell","Desculpe, você não tem dinheiro suficiente!"))
		pausa()

def venderItemUnico(valor, idItem, nomeItem, generoFrase):
	if(item.retornaPosicaoItem(idItem) != -1):
		if(generoFrase == 0):
			print(mundo.recebeFala(0,0,"Gaben Ewell","Você quer me vender esse " + nomeItem + "?\n| Posso te pagar " + str(valor) + "$ por ele\n| \n| 1 - Sim\n| 2 - Não"))
		else:
			print(mundo.recebeFala(0,0,"Gaben Ewell","Você quer me vender essa " + nomeItem + "?\n| Posso te pagar " + str(valor) + "$ por ela\n| \n| 1 - Sim\n| 2 - Não"))
		confirma = int(input("confirmação> "))
		if(confirma == 1):
			item.setarItem(item.retornaPosicaoItem(idItem),0,0)
			heroi.gold = heroi.gold + valor
			print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado, é bom fazer negócios com você!\n| Aqui estão seus " + str(valor) + "$"))
			pausa()
	else:
		if(generoFrase == 0):
			print(mundo.recebeFala(0,0,"Gaben Ewell","Hmm, você não possui nenhum " + nomeItem + " no seu inventário."))
		else:
			print(mundo.recebeFala(0,0,"Gaben Ewell","Hmm, você não possui nenhuma " + nomeItem + " no seu inventário."))
		pausa()

def comprarItemLote(preco, idItem, nomeItem, nomeItemPlural, generoFrase):
	if(heroi.gold >= preco): 
		if(item.retornaPosicaoItem(idItem) != -1): # Possui o item no inventário
			try:
				if(generoFrase == 0):
					print(mundo.recebeFala(1,heroi.gold,"Gaben Ewell","Quantos " + nomeItemPlural + " você quer?\n| Ou digite '0' para cancelar"))
				else:
					print(mundo.recebeFala(1,heroi.gold,"Gaben Ewell","Quantas " + nomeItemPlural + " você quer?\n| Ou digite '0' para cancelar"))
				quantPoc = int(input("quantidade> "))
				if(quantPoc > 1 and heroi.gold >= preco*quantPoc):
					if(generoFrase == 0):
						print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado!\n| Aqui estão seus " + nomeItemPlural + "."))
					else:
						print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado!\n| Aqui estão suas " + nomeItemPlural + "."))
					item.alterarQuantidadeItem(0, item.retornaPosicaoItem(idItem), quantPoc)
					heroi.gold = heroi.gold - preco*quantPoc
					pausa()
				elif(quantPoc > 1 and heroi.gold < preco*quantPoc):
					print(mundo.recebeFala(0,0,"Gaben Ewell","Desculpe, você não tem dinheiro suficiente para\n| esse número de " + nomeItemPlural + "!"))
					pausa()
				elif(quantPoc == 1):
					if(generoFrase == 0):
						print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado!\n| Aqui está seu " + nomeItem + "."))
					else:
						print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado!\n| Aqui está sua " + nomeItem + "."))
					item.alterarQuantidadeItem(0, item.retornaPosicaoItem(idItem), 1)
					heroi.gold = heroi.gold - preco
					pausa()
				else:
					print(mundo.recebeFala(0,0,"Gaben Ewell","Cancelando..."))
					pausa()
			except ValueError:
				print(mundo.recebeFala(0,0,"Gaben Ewell","Cancelando..."))
				pausa()
		else: # Não possui no inventário
			if(item.retornaPrimeiroSlotVazio() != -1): # Possui espaço no inventário
				try:
					if(generoFrase == 0):
						print(mundo.recebeFala(1,heroi.gold,"Gaben Ewell","Quantos " + nomeItemPlural + " você quer?\n| Ou digite '0' para cancelar"))
					else:
						print(mundo.recebeFala(1,heroi.gold,"Gaben Ewell","Quantas " + nomeItemPlural + " você quer?\n| Ou digite '0' para cancelar"))
					quantPoc = int(input("quantidade> "))
					if(quantPoc > 1 and heroi.gold >= preco*quantPoc):
						if(generoFrase == 0):
							print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado!\n| Aqui estão seus " + nomeItemPlural + "."))
						else:
							print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado!\n| Aqui estão suas " + nomeItemPlural + "."))
						item.setarItem(item.retornaPrimeiroSlotVazio(), idItem, quantPoc)
						heroi.gold = heroi.gold - preco*quantPoc
						pausa()
					elif(quantPoc > 1 and heroi.gold < preco*quantPoc):
						print(mundo.recebeFala(0,0,"Gaben Ewell","Desculpe, você não tem dinheiro suficiente para\n| esse número de " + nomeItemPlural + "!"))
						pausa()
					elif(quantPoc == 1):
						if(generoFrase == 0):
							print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado!\n| Aqui está seu " + nomeItem + "."))
						else:
							print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado!\n| Aqui está sua " + nomeItem + "."))
						item.setarItem(item.retornaPrimeiroSlotVazio(), idItem, 1)
						heroi.gold = heroi.gold - preco
						pausa()
					else:
						print(mundo.recebeFala(0,0,"Gaben Ewell","Cancelando..."))
						pausa()
				except ValueError:
					print(mundo.recebeFala(0,0,"Gaben Ewell","Cancelando..."))
					pausa()
			else: # Não possui espaço no inventário
				print(mundo.recebeFala(0,0,"Gaben Ewell","Lamento!\n| Você não tem espaço no intenvário!"))
				pausa()
	else:
		print(mundo.recebeFala(0,0,"Gaben Ewell","Desculpe, você não tem dinheiro suficiente para comprar " + nomeItemPlural + "!"))
		pausa()
		
def venderItemLote(valor, idItem, nomeItem, nomeItemPlural, generoFrase):
	if(item.retornaPosicaoItem(idItem) != -1): # Verifica se possui pelo menos UM item
		if(generoFrase == 0):
			print(mundo.recebeFala(0,0,"Gaben Ewell","Você quer me vender quantos " + nomeItemPlural + "?\n| Posso te pagar " + str(valor) + "$ por cada\n+-----------\n| Você tem " + str(item.retornaQuantidadeItem(idItem)) + " " + nomeItemPlural))
		else:
			print(mundo.recebeFala(0,0,"Gaben Ewell","Você quer me vender quantas " + nomeItemPlural + "?\n| Posso te pagar " + str(valor) + "$ por cada\n+-----------\n| Você tem " + str(item.retornaQuantidadeItem(idItem)) + " " + nomeItemPlural))
		try:
			tmpQuant = int(input("venda> "))
			if(tmpQuant <= item.retornaQuantidadeItem(idItem) and tmpQuant > 0):
				if(tmpQuant > 1):
					print(mundo.recebeFala(0,0,"Gaben Ewell","Você que me vender " + str(tmpQuant) + " " + nomeItemPlural + "?\n| Posso te pagar " + str(valor * tmpQuant) + "$ por tudo.\n| 1 - Sim\n| 2 - Não"))
				elif(tmpQuant == 1 and generoFrase == 0):
					print(mundo.recebeFala(0,0,"Gaben Ewell","Você que me vender 1 " + nomeItem + "?\n| Posso te pagar " + str(valor * tmpQuant) + "$ por ele.\n| 1 - Sim\n| 2 - Não"))
				elif(tmpQuant == 1 and generoFrase == 1):
					print(mundo.recebeFala(0,0,"Gaben Ewell","Você que me vender 1 " + nomeItem + "?\n| Posso te pagar " + str(valor * tmpQuant) + "$ por ela.\n| 1 - Sim\n| 2 - Não"))
				tmpConfirma = input("venda> ")
				if(tmpConfirma == "1"):
					if(item.retornaQuantidadeItem(idItem) == tmpQuant):
						item.setarItem(item.retornaPosicaoItem(idItem),0,0)
					else:
						item.alterarQuantidadeItem(1,item.retornaPosicaoItem(idItem),tmpQuant)
					heroi.gold = heroi.gold + (valor * tmpQuant)
					print(mundo.recebeFala(0,0,"Gaben Ewell","Obrigado, é bom fazer negócios com você!\n| Aqui estão seus " + str(valor * tmpQuant) + "$"))
				else:
					print(mundo.recebeFala(0,0,"Gaben Ewell","Cancelando..."))
			elif(tmpQuant > item.retornaQuantidadeItem(idItem)):
				if(generoFrase == 0):
					print(mundo.recebeFala(0,0,"Gaben Ewell","Você não tem tantos " + nomeItemPlural + " assim."))
				else:
					print(mundo.recebeFala(0,0,"Gaben Ewell","Você não tem tantas " + nomeItemPlural + " assim."))
			else:
				print(mundo.recebeFala(0,0,"Gaben Ewell","Cancelando..."))
		except ValueError:
			print(mundo.recebeFala(0,0,"Gaben Ewell","Cancelando..."))
		pausa()
	else:
		if(generoFrase == 0):
			print(mundo.recebeFala(0,0,"Gaben Ewell","Hmm, você não possui nenhum " + nomeItem + " no seu inventário."))
		else:
			print(mundo.recebeFala(0,0,"Gaben Ewell","Hmm, você não possui nenhuma " + nomeItem + " no seu inventário."))
		pausa()

def compraLoja():
	menuCompra = ""
	menuCompra2 = ""
	if(mundo.retornaAssaltoPousada() == 1):
		print(mundo.recebeFala(0,0,"Gaben Ewell","A loja foi saqueada recentemente.\n| Tive que aumentar um pouco os preços\n| para poder me manter, espero que compreenda."))
		pausa()
	while(menuCompra != "0"):
		menuCompra2 = ""
		print(mundo.recebeFala(0,0,"Gaben Ewell","Bem-vindo a minha loja!!\n| Você deseja:\n|  1 - comprar\n|  2 - vender\n|  0 - sair"))
		menuCompra = input("loja> ")
		while(menuCompra == "1" and menuCompra2 != "0"):
			# ---------------
			# MENU DE COMPRA
			#---------------
			if(mundo.retornaAssaltoPousada() != 1):
				print(mundo.recebeFala(1,heroi.gold,"Gaben Ewell","O que deseja comprar?\n| 1 - Poção - 6$\n| 2 - Super poção - 11$\n| 3 - Repelente - 10$\n| 4 - Antídoto - 5$\n| 5 - Espada - 32$\n| 6 - Escudo - 28$\n| 7 - Binóculos - 20$\n| 8 - Totem da imortalidade - 80$\n| 9 - Totem da regeneração - 70$\n| 0 - Sair"))
				menuCompra2 = input("comprar> ")
				if(menuCompra2 == "1"): # Comprando POÇÃO
					comprarItemLote(6, 1, "poção", "poções", 1)
				elif(menuCompra2 == "2"): # Comprando SUPER POÇÃO
					comprarItemLote(11, 2, "super poção", "super poções", 1)
				elif(menuCompra2 == "3"): # Comprando REPELENTE
					comprarItemLote(10, 8, "repelente", "repelentes", 0)
				elif(menuCompra2 == "4"): # Comprando ANTÍDOTO
					comprarItemLote(5, 12, "antídoto", "antídotos", 0)
				elif(menuCompra2 == "5"): # Comprando ESPADA
					comprarItemUnico(32, 4, "espada", 1)
				elif(menuCompra2 == "6"): # Comprando ESCUDO
					comprarItemUnico(28, 9, "escudo", 0)
				elif(menuCompra2 == "7"): # Comprando BINÓCULOS
					comprarItemUnico(20, 10, "binóculos", 0)
				elif(menuCompra2 == "8"): # Comprando TOTEM DA IMORTALIDADE
					comprarItemUnico(80, 13, "totem da imortalidade", 0)
				elif(menuCompra2 == "9"): # Comprando TOTEM DA REGENERAÇÃO
					comprarItemUnico(70, 13, "totem da regeneração", 0)
			else:
				print(mundo.recebeFala(1,heroi.gold,"Gaben Ewell","O que deseja comprar?\n| 1 - Poção - 8$\n| 2 - Super poção - 15$\n| 3 - Repelente - 13$\n| 4 - Antídoto - 7$\n| 5 - Espada - 43$\n| 6 - Escudo - 37$\n| 7 - Binóculos - 27$\n| 8 - Totem da imortalidade - 106$\n| 9 - Totem da regeneração - 93$\n| 0 - Sair"))
				menuCompra2 = input("comprar> ")
				if(menuCompra2 == "1"): # Comprando POÇÃO
					comprarItemLote(8, 1, "poção", "poções", 1)
				elif(menuCompra2 == "2"): # Comprando SUPER POÇÃO
					comprarItemLote(15, 2, "super poção", "super poções", 1)
				elif(menuCompra2 == "3"): # Comprando REPELENTE
					comprarItemLote(13, 8, "repelente", "repelentes", 0)
				elif(menuCompra2 == "4"): # Comprando ANTÍDOTO
					comprarItemLote(7, 12, "antídoto", "antídotos", 0)
				elif(menuCompra2 == "5"): # Comprando ESPADA
					comprarItemUnico(43, 4, "espada", 1)
				elif(menuCompra2 == "6"): # Comprando ESCUDO
					comprarItemUnico(37, 9, "escudo", 0)
				elif(menuCompra2 == "7"): # Comprando BINÓCULOS
					comprarItemUnico(27, 10, "binóculos", 0)
				elif(menuCompra2 == "8"): # Comprando TOTEM DA IMORTALIDADE
					comprarItemUnico(106, 13, "totem da imortalidade", 0)
				elif(menuCompra2 == "9"): # Comprando TOTEM DA REGENERAÇÃO
					comprarItemUnico(93, 13, "totem da regeneração", 0)
		while(menuCompra == "2" and menuCompra2 != "0"):
			# ---------------
			# MENU DE VENDA
			#---------------
			print(mundo.recebeFala(0,0,"Gaben Ewell","O que você quer vender?\n| Conforme está na parede, o quanto pago por cada item\n| que estou comprando\n| 1 - Espada - 20$\n| 2 - Porrete - 4$\n| 3 - Poção - 3$\n| 4 - Super poção 5$\n|\n| 0 - Voltar"))
			menuCompra2 = input("vender> ")
			if(menuCompra2 == "1"):
				venderItemUnico(20, 4, "espada", 1)
			elif(menuCompra2 == "2"):
				venderItemUnico(4, 11, "porrete", 0)
			elif(menuCompra2 == "3"):
				venderItemLote(3,1,"poção","poções",1)
			elif(menuCompra2 == "4"):
				venderItemLote(5,2,"super poção","super poções",1)
				
#---------------------
#
# VENDEDOR MISTERIOSO
#
#--------------------

def comprarItemMistUnico(valor, idItem, nomeItem, generoFrase):
	if(heroi.gold >= valor):
		if(item.retornaPrimeiroSlotVazio() != -1):
			if(generoFrase == 0):
				print(mundo.recebeFala(0,0,"Misterioso","Bom fazer negócios com você!\n| Aqui está seu " + nomeItem))
			else:
				print(mundo.recebeFala(0,0,"Misterioso","Bom fazer negócios com você!\n| Aqui está sua " + nomeItem))
			item.setarItem(item.retornaPrimeiroSlotVazio(), idItem, 1)
			heroi.gold = heroi.gold - valor
			pausa()
		else:
			print(mundo.recebeFala(0,0,"Misterioso","Hmmm...\n| Você quer carregar isso aonde?!"))
			pausa()
	else:
		print(mundo.recebeFala(0,0,"Misterioso","Sem dinheiro?! Dê o fora e não me atrapalhe"))
		pausa()

def comprarItemMistLote(preco, idItem, nomeItem, nomeItemPlural, generoFrase):
	if(heroi.gold >= preco): 
		if(item.retornaPosicaoItem(idItem) != -1): # Possui o item no inventário
			try:
				if(generoFrase == 0):
					print(mundo.recebeFala(1,heroi.gold,"Misterioso","Quantos " + nomeItemPlural + " você quer?\n| Ou digite '0' para cancelar"))
				else:
					print(mundo.recebeFala(1,heroi.gold,"Misterioso","Quantas " + nomeItemPlural + " você quer?\n| Ou digite '0' para cancelar"))
				quantPoc = int(input("quantidade> "))
				if(quantPoc > 1 and heroi.gold >= preco*quantPoc):
					if(generoFrase == 0):
						print(mundo.recebeFala(0,0,"Misterioso","Bom fazer negócios com você!\n| Aqui estão seus " + nomeItemPlural + "."))
					else:
						print(mundo.recebeFala(0,0,"Misterioso","Bom fazer negócios com você!\n| Aqui estão suas " + nomeItemPlural + "."))
					item.alterarQuantidadeItem(0, item.retornaPosicaoItem(idItem), quantPoc)
					heroi.gold = heroi.gold - preco*quantPoc
					pausa()
				elif(quantPoc > 1 and heroi.gold < preco*quantPoc):
					print(mundo.recebeFala(0,0,"Misterioso","Quer me passar a perna? Você não tem dinheiro suficiente para\n| esse número de " + nomeItemPlural + "!"))
					pausa()
				elif(quantPoc == 1):
					if(generoFrase == 0):
						print(mundo.recebeFala(0,0,"Misterioso","Bom fazer negócios com você!\n| Aqui está seu " + nomeItem + "."))
					else:
						print(mundo.recebeFala(0,0,"Misterioso","Bom fazer negócios com você!\n| Aqui está sua " + nomeItem + "."))
					item.alterarQuantidadeItem(0, item.retornaPosicaoItem(idItem), 1)
					heroi.gold = heroi.gold - preco
					pausa()
				else:
					print(mundo.recebeFala(0,0,"Misterioso","Cancelando..."))
					pausa()
			except ValueError:
				print(mundo.recebeFala(0,0,"Misterioso","Cancelando..."))
				pausa()
		else: # Não possui o item no inventário
			if(item.retornaPrimeiroSlotVazio() != -1): # Possui espaço no inventário
				try:
					if(generoFrase == 0):
						print(mundo.recebeFala(1,heroi.gold,"Misterioso","Quantos " + nomeItemPlural + " você quer?\n| Ou digite '0' para cancelar"))
					else:
						print(mundo.recebeFala(1,heroi.gold,"Misterioso","Quantas " + nomeItemPlural + " você quer?\n| Ou digite '0' para cancelar"))
					quantPoc = int(input("quantidade> "))
					if(quantPoc > 1 and heroi.gold >= preco*quantPoc):
						if(generoFrase == 0):
							print(mundo.recebeFala(0,0,"Misterioso","Bom fazer negócios com você!\n| Aqui estão seus " + nomeItemPlural + "."))
						else:
							print(mundo.recebeFala(0,0,"Misterioso","Bom fazer negócios com você!\n| Aqui estão suas " + nomeItemPlural + "."))
						item.setarItem(item.retornaPrimeiroSlotVazio(), idItem, quantPoc)
						heroi.gold = heroi.gold - preco*quantPoc
						pausa()
					elif(quantPoc > 1 and heroi.gold < preco*quantPoc):
						print(mundo.recebeFala(0,0,"Misterioso","Quer me passar a perna? Você não tem dinheiro suficiente para\n| esse número de " + nomeItemPlural + "!"))
						pausa()
					elif(quantPoc == 1):
						if(generoFrase == 0):
							print(mundo.recebeFala(0,0,"Misterioso","Bom fazer negócios com você!\n| Aqui está seu " + nomeItem + "."))
						else:
							print(mundo.recebeFala(0,0,"Misterioso","Bom fazer negócios com você!\n| Aqui está sua " + nomeItem + "."))
						item.setarItem(item.retornaPrimeiroSlotVazio(), idItem, 1)
						heroi.gold = heroi.gold - preco
						pausa()
					else:
						print(mundo.recebeFala(0,0,"Misterioso","Cancelando..."))
						pausa()
				except ValueError:
					print(mundo.recebeFala(0,0,"Misterioso","Cancelando..."))
					pausa()
			else: # Não possui espaço no inventário
				print(mundo.recebeFala(0,0,"Misterioso","Hmmm...\n| Você quer carregar isso aonde?!"))
				pausa()
	else:
		print(mundo.recebeFala(0,0,"Misterioso","É simples! Sem dinheiro, sem " + nomeItemPlural + "!"))
		pausa()
				
def compraVendedorMisterioso():
	menuCompra = ""
	while(menuCompra != "0"):
		print(mundo.recebeFala(1,heroi.gold,"Misterioso","Olá, eu só vendo coisas\n| O que quer?\n|  1 - Espada - $28\n|  2 - Espada Forjada - $40\n|  3 - Manopla - $29\n|  4 - Manopla Revestida - $36\n|  5 - Super poção - $8\n|  6 - Totem da Imortalidade - $55\n|  7 - Totem da Regeneração - $38\n|  0 - Sair"))
		menuCompra = input("comprar> ")
		if(menuCompra == "1"):
			comprarItemMistUnico(28,4,"espada",1)
		elif(menuCompra == "2"):
			comprarItemMistUnico(40,6,"espada forjada",1)
		elif(menuCompra == "3"):
			comprarItemMistUnico(29,5,"manopla",1)
		elif(menuCompra == "4"):
			comprarItemMistUnico(36,7,"manopla revestida",1)
		elif(menuCompra == "5"):
			comprarItemMistLote(8,2,"super poção","super poções",1)
		elif(menuCompra == "6"):
			comprarItemMistUnico(55,13,"totem da imortalidade",0)
		elif(menuCompra == "7"):
			comprarItemMistUnico(38,14,"totem da regeneração",0)
		elif(menuCompra == "0"):
			print(mundo.recebeFala(0,0,"Misterioso","Nos vemos por aí"))
			mundo.moverNPC(1)
		else:
			print(mundo.recebeFala(0,0,"Misterioso","Fale o que quer de uma vez!"))

def falarPousada():
	menuPou = ""
	while(menuPou != "2"):
		print(mundo.recebeFala(0,0,"Dona Gertrudes","Olá jovenzinho. Bem-vindo a Pousada Heróica!\n| Você gostaria de se hospedar?\n| 1 - Sim\n| 2 - Não"))
		menuPou = input("pousada> ")
		if(menuPou == "1"):
			print(mundo.recebeFala(0,0,"Dona Gertrudes","Certo, tome. Esta é a chave do quarto " + mundo.retornaNumeroQuarto() + " pode ficar por lá!"))
			pausa()
			print("\n\n")
			if(mundo.retornaAssaltoPousada() != 1):
				if(heroi.HP < heroi.maxHP):
					heroi.HP = heroi.maxHP
					print("Você acordou descansado.")
				elif(heroi.HP == heroi.maxHP):
					print("Você tirou um cochilo.")
				else:
					print("Você sonhou acordado, pensando em fadas.")
				heroi.passosRestantesVeneno = 0 # cura o veneno
			else:
				if(heroi.gold > 12):
					heroi.gold = heroi.gold - 12
					print("Você não conseguiu dormir direito e notou que foi roubado!")
				else:
					print("Você não conseguiu dormir direito e achou\nter visto alguém no quarto durante a estadia!")
			menuPou = "2"
			if(mundo.horas >= 18):
				print("Os pássaros cantam em um novo dia!")
				mundo.avancarUmDia()
			else:
				print("Algumas horas se passaram!")
				mundo.adicionarTempo(4,0)
		elif(menuPou == "2"):
			print(mundo.recebeFala(0,0,"Dona Gertrudes","Tudo bem, volte outra hora"))
		else:
			print(mundo.recebeFala(0,0,"Dona Gertrudes","Como? Não entendi."))

def imprimirHud(isBatalha):
	if(isBatalha == 0):
		print("")
		print(heroi.nome + " | HP: " + str(heroi.HP) + "/" + str(heroi.maxHP) + " | " + mundo.retornarHorario())
		print("X: " + str(heroi.posX) + " | Y: " + str(heroi.posY) + " - " + mundo.lerPosicaoAtual(heroi.posX, heroi.posY))
		print("O que devo fazer agora?")
	else:
		print("")
		print(heroi.nome + " - HP: " + str(heroi.HP) + "/" + str(heroi.maxHP) + " VS " + inimigo.nome + " - HP: " + str(inimigo.HP) + "/" + str(inimigo.maxHP))
		print("atk - atacar | fug - fugir | bbp - beber poção | bbsp - beber super poção\nO que fazer agora?")
		
def getDadoPocao(qualDado, tipoPocao):
	if(tipoPocao == 1): # poção normal
		posPocao = item.retornaPosicaoItem(1)
		if(posPocao != -1):
			if(qualDado == 1): # retorna posição da poção
				return posPocao
			elif(qualDado == 2): # retorna a quantidade de poção
				return item.inventario[posPocao].quantidade
			else: # retorna o aumento de HP da poção
				return item.inventario[posPocao].curaHP
		else:
			return 0
	else: # Super poção
		posPocao = item.retornaPosicaoItem(2)
		if(posPocao != -1):
			if(qualDado == 1): # retorna posição da poção
				return posPocao
			elif(qualDado == 2): # retorna a quantidade de poção
				return item.inventario[posPocao].quantidade
			else: # retorna o aumento de HP da poção
				return item.inventario[posPocao].curaHP
		else:
			return 0

def totemRegen():
	if((heroi.HP > 1 and heroi.HP < heroi.maxHP) and item.possuiItem(14) == 1):
		print("O totem da regeneração curou + 1HP")
		heroi.HP = heroi.HP + 1
			

def batalha():
	inimigo.setarInimigo(heroi.level, mundo.horas)
	comando = ""
	while(heroi.HP > 0 and inimigo.HP > 0 and comando != "fug"):
		imprimirHud(1)
		comando = input("> ")
		
		if(comando == "atk"): # atacar
			inimigo.receberDano(heroi.atacar(item.retornaAumentoAtk(), item.retornaAumentoForca()), heroi.isDanoCrit)
			if(inimigo.HP > 0):
				heroi.receberDano(inimigo.atacar(), inimigo.isDanoCrit, inimigo.isVeneno(), item.retornaAumentoArmadura())
			heroi.verificaVeneno()
			totemRegen()
			mundo.adicionarTempo(0,2)
				
		elif(comando == "fug"): # fugir
			chance = heroi.fugirBatalha()
			if(chance == 1): # conseguiu escapar
				print(heroi.nome + " escapou da batalha!")
				heroi.verificaVeneno()
				totemRegen()
				mundo.adicionarTempo(0,2)
			else: # NÃO conseguiu escapar
				comando = ""
				print(heroi.nome + " não conseguiu escapar!")
				heroi.receberDano(inimigo.atacar(), inimigo.isDanoCrit, inimigo.isVeneno(), item.retornaAumentoArmadura())
				heroi.verificaVeneno()
				totemRegen()
				mundo.adicionarTempo(0,2)
				
		elif(comando == "bbp"): # beber poção
			if(heroi.beberPocao(0, getDadoPocao(2, 1), getDadoPocao(3, 1))):
				item.alterarQuantidadeItem(1, getDadoPocao(1, 1), 1)
				heroi.receberDano(inimigo.atacar(), inimigo.isDanoCrit, inimigo.isVeneno(), item.retornaAumentoArmadura())
				heroi.verificaVeneno()
				totemRegen()
				mundo.adicionarTempo(0,2)
				
		elif(comando == "bbsp"): # beber super poção
			if(heroi.beberPocao(1, getDadoPocao(2, 2), getDadoPocao(3, 2))):
				item.alterarQuantidadeItem(1, getDadoPocao(1, 2), 1)
				heroi.receberDano(inimigo.atacar(), inimigo.isDanoCrit, inimigo.isVeneno(), item.retornaAumentoArmadura())
				heroi.verificaVeneno()
				totemRegen()
				mundo.adicionarTempo(0,2)

		else:
			print("Não é hora de '" + comando + "'! Você está no meio de uma batalha!")
		
		if(heroi.HP < 1):
			print("O inimigo, vendo seu corpo no chão, dá as costas e vai embora")
			
	if(comando != "fug" and heroi.HP > 0):
		if(inimigo.HP > -15):
			print("---\n" + heroi.nome + " derrotou o " + inimigo.nome + "!")
			print("Você ganhou " + str(inimigo.retornarExpGanha()) + " de experiência e " + str(inimigo.retornarGoldGanho()) + " de ouro!")
			heroi.exp = heroi.exp + inimigo.retornarExpGanha()
			heroi.gold = heroi.gold + inimigo.retornarGoldGanho()
			heroi.subirNivel()
			if(inimigo.chanceDroparPocao() == 1): # Se o inimigo for dropar uma poção
				if(item.retornaPosicaoItem(1) != -1): # Verifica se o herói tem poções no inventário
					item.alterarQuantidadeItem(0, item.retornaPosicaoItem(1), 1)
					print("O inimigo dropou uma poção!")
				else: # Se o herói não tiver poções
					if(item.retornaPrimeiroSlotVazio() != -1): # Verifica se há espaço no inventário
						item.setarItem(item.retornaPrimeiroSlotVazio(), 1, 1) # Adiciona uma nova poção
						print("O inimigo dropou uma poção!")
					else: # Se não tiver espaço
						print("O inimigo dropou uma poção, mas você não tem espaço no inventário!")
			mundo.adicionarTempo(0,2)
		else:
			print("---\n" + heroi.nome + " derrotou o " + inimigo.nome + " com um poderoso golpe!")
			print("Você ganhou " + str(inimigo.retornarExpGanha() * 2) + " de experiência e " + str(inimigo.retornarGoldGanho() * 2) + " de ouro!")
			heroi.exp = heroi.exp + inimigo.retornarExpGanha() * 2
			heroi.gold = heroi.gold + inimigo.retornarGoldGanho() * 2
			heroi.subirNivel()
			if(inimigo.chanceDroparPocao() == 1): # Se o inimigo for dropar uma poção
				if(item.retornaPosicaoItem(2) != -1): # Verifica se o herói tem poções no inventário
					item.alterarQuantidadeItem(0, item.retornaPosicaoItem(2), 1)
					print("O inimigo dropou uma super poção!")
				else: # Se o herói não tiver poções
					if(item.retornaPrimeiroSlotVazio() != -1): # Verifica se há espaço no inventário
						item.setarItem(item.retornaPrimeiroSlotVazio(), 2, 1) # Adiciona uma nova poção
						print("O inimigo dropou uma super poção!")
					else: # Se não tiver espaço
						print("O inimigo dropou uma super poção, mas você não tem espaço no inventário!")
			mundo.adicionarTempo(0,2)


def verificaPreenchimentoMapa():
	if(heroi.posX == mundo.posPortaoX and heroi.posY == mundo.posPortaoY):
		heroi.achouPortao = 1
	if(heroi.posX == mundo.posNPCX and heroi.posY == mundo.posNPCY):
		heroi.achouLoja = 1
	if(heroi.posX == mundo.posChaveX and heroi.posY == mundo.posChaveY):
		heroi.achouChave = 1
	if(heroi.posX == mundo.posPouX and heroi.posY == mundo.posPouY):
		heroi.achouPousada = 1

	# Verifica se está com a fada
	if(heroi.posX == mundo.posFadX and heroi.posY == mundo.posFadY):
		if(not item.possuiItem(3)):
			print(mundo.recebeFala(0,0,"Fada","Ei, você me achou!\n| Eu sou uma fada. Vou lhe dar um pouco de vida.\n| Outra ajuda, a chave esta a" + mundo.returnDirecaoChave(heroi.posX,heroi.posY)))
		else:
			print(mundo.recebeFala(0,0,"Fada","Ei, você me achou!\n| Eu sou uma fada. Vou lhe dar um pouco de vida.\n| Essa chave foi forjada por fadas, sabia?"))
		heroi.HP = heroi.HP + 30
		pausa()
		if(heroi.HP > heroi.maxHP):
			print(mundo.recebeFala(0,0,"Fada","Você está com sobrecura agora.\n| É um dom especial das fadas, hehe"))
			pausa()
		mundo.moverNPC(2)

def pausa():
	wst = input("Pressione ENTER para continuar.")
	print("\n\n")

#------------------------------------------
#	   Código do jogo
#------------------------------------------

# Titúlo feito em http://www.network-science.de/ascii/
print("        AAA        dd                        tt                          \n       AAAAA       dd vv   vv   eee  nn nnn  tt    uu   uu rr rr    eee  \n      AA   AA  dddddd  vv vv  ee   e nnn  nn tttt  uu   uu rrr  r ee   e \n      AAAAAAA dd   dd   vvv   eeeee  nn   nn tt    uu   uu rr     eeeee  \n      AA   AA  dddddd    v     eeeee nn   nn  tttt  uuuu u rr      eeeee \n                                                                         \n                        GGGG                             \n                       GG  GG   aa aa mm mm mmmm    eee  \n                      GG       aa aaa mmm  mm  mm ee   e \n                      GG   GG aa  aaa mmm  mm  mm eeeee  \n                       GGGGGG  aaa aa mmm  mm  mm  eeeee \n                                                               v0.8")

heroi.nome = input("Digite um nome para o herói:\n> ")

if(heroi.verificarPossuiJogoSalvo(heroi.nome)):
	print("Já existe um jogo salvo com esse nome, gostaria de abrí-lo?")
	decisao = ""
	while(decisao != "s" and decisao != "S" and decisao != "n" and decisao != "N"):
		decisao = input("(s/n) | > ")
		if(decisao == "s" or decisao == "S"):
			carregarJogo()
			heroi.assistiuCutscene = 1
		elif(decisao == "n" or decisao == "N"):
			print("Um novo jogo será iniciado")
		else:
			print("Digite apenas S ou N")

# Apresenta a história do jogo em uma cutscene
if(heroi.assistiuCutscene == 0):
	print("\n*HISTÓRIA*\n\nVocê acorda-se deitado no chão. Sua cabeça dói um pouco.\nVocê levanta a cabeça olhando em volta, tentando reconhecer o local.\nVocê se encontra em um local que nunca viu antes.")
	pausa()
	print("Ao olhar em volta, você vê o que parece ser uma pequena cidade, mas cercada por um muro extremamente alto.\nVocê também consegue ver o que parece ser uma loja e uma pousada ao alcance da vista")
	pausa()
	print("Um leve medo lhe corre pela espinha, pois nas sombras alguns monstros parecem observar você")
	pausa()
	print("Você consegue ver um portão trancado em uma extremidade, você imagina que deve procurar por uma chave para abrí-lo")
	pausa()

menu = ""
while(menu != "sair" and heroi.HP > 0):
	imprimirHud(0)
	menu = input("> ")

	if(menu == "salvarJogo"):
		heroi.salvarJogo(mundo.posChaveX, mundo.posChaveY, mundo.posPortaoX, mundo.posPortaoY, mundo.posNPCX, mundo.posNPCY, mundo.posMNX, mundo.posMNY, mundo.posFadX, mundo.posFadY, mundo.posPouX, mundo.posPouY, item.inventario[0].itemID, item.inventario[0].quantidade, item.inventario[1].itemID, item.inventario[1].quantidade, item.inventario[2].itemID, item.inventario[2].quantidade, item.inventario[3].itemID, item.inventario[3].quantidade, mundo.dias, mundo.horas, mundo.minutos)

	elif (menu == "abrirJogo" or menu == "carregarJogo"):
		carregarJogo()

	elif(menu == "goN" or menu == "gon" or menu == "go n"):
		if(heroi.caminhar(1, mundo.limiteY) == 1):
			mundo.sortearItemFalso()
			verificaPreenchimentoMapa()
			heroi.verificaVeneno()
			totemRegen()
			mundo.adicionarTempo(0,10)
			if(heroi.chanceBatalha() == 1 and heroi.passosRestantesRepel == 0):
				batalha()

	elif(menu == "goS" or menu == "gos" or menu == "go s"):
		if(heroi.caminhar(2, mundo.limiteY) == 1):
			mundo.sortearItemFalso()
			verificaPreenchimentoMapa()
			heroi.verificaVeneno()
			totemRegen()
			mundo.adicionarTempo(0,10)
			if(heroi.chanceBatalha() == 1 and heroi.passosRestantesRepel == 0):
				batalha()

	elif(menu == "goL" or menu == "gol" or menu == "go l"):
		if(heroi.caminhar(3, mundo.limiteX) == 1):
			mundo.sortearItemFalso()
			verificaPreenchimentoMapa()
			heroi.verificaVeneno()
			totemRegen()
			mundo.adicionarTempo(0,5)
			if(heroi.chanceBatalha() == 1 and heroi.passosRestantesRepel == 0):
				batalha()

	elif(menu == "goO" or menu == "goo" or menu == "go o"):
		if(heroi.caminhar(4, mundo.limiteX) == 1):
			mundo.sortearItemFalso()
			verificaPreenchimentoMapa()
			heroi.verificaVeneno()
			totemRegen()
			mundo.adicionarTempo(0,10)
			if(heroi.chanceBatalha() == 1 and heroi.passosRestantesRepel == 0):
				batalha()

	elif(menu == "go"):
		print("Você precisa específicar uma direção!\ngon - caminhar norte\ngos - caminhar sul\ngol - caminhar leste\ngoo - caminhar oeste")

	elif(menu == "falar"):
		if(heroi.posX == mundo.posNPCX and heroi.posY == mundo.posNPCY):
			compraLoja()
		elif(heroi.posX == mundo.posMNX and heroi.posY == mundo.posMNY):
			compraVendedorMisterioso()
		elif(heroi.posX == mundo.posPouX and heroi.posY == mundo.posPouY):
			falarPousada()
		else:
			print("Você não está próximo de ninguém!")

	elif(menu == "pegar"):
		if(mundo.isItemFalso == 0):			
			if(heroi.posX == mundo.posChaveX and heroi.posY == mundo.posChaveY):
				if(item.retornaPrimeiroSlotVazio() != -1):
					item.setarItem(item.retornaPrimeiroSlotVazio(), 3, 1)
					print("Você pegou a chave!\nAgora encontre o portão!")
					mundo.moverChave()
					mundo.adicionarTempo(0,2)
				else:
					print("O seu inventário está cheio!\nSe desfaça de algum item usando 'largar'")
			else:
				print("Você não vê nada pelo chão!")
		else:
			if(random.randint(1,12) == 1): # ACHOU POÇÃO
				if(item.retornaPosicaoItem(1) != -1): # Verifica se o herói tem poções no inventário
					item.alterarQuantidadeItem(0, item.retornaPosicaoItem(1), 1)
					print("Você achou uma poção lacrada no chão. Você pegou ela.")
					mundo.adicionarTempo(0,2)
				else: # Se o herói não tiver poções
					if(item.retornaPrimeiroSlotVazio() != -1): # Verifica se há espaço no inventário
						item.setarItem(item.retornaPrimeiroSlotVazio(), 1, 1) # Adiciona uma nova poção
						print("Você achou uma poção lacrada no chão. Você pegou ela.")
					else: # Se não tiver espaço
						print("Você achou uma poção lacrada no chão, mas você não tem espaço no inventário!")
					mundo.adicionarTempo(0,2)
			elif(random.randint(1,4) == 1): # ACHOU PORRETE
				if(item.retornaPrimeiroSlotVazio() != -1):
					item.setarItem(item.retornaPrimeiroSlotVazio(), 11, 1)
					print("Você achou um porrete no chão. Você pegou ele.")
				else:
					print("Você achou um porrete no chão, mas não tem espaço no inventário.")
				mundo.adicionarTempo(0,2)
			elif(random.randint(1,5) == 1): # ACHOU POÇÃO VAZIA
				print("Você achou um frasco de poção. Está vazio!")
				mundo.adicionarTempo(0,2)
			elif(random.randint(1,6) == 1): # ACHOU UM INIMIGO
				print("Era apenas uma pedra brilhosa, um inimigo saltou em você!")
				mundo.adicionarTempo(0,2)
				batalha()
			else: # NÃO ACHOU NADA
				print("Você vasculhou em volta, mas o item parece ter sumido!")
				mundo.adicionarTempo(0,2)
			mundo.isItemFalso = 0 # Remove o item do chão

	elif(menu == "abrir"):
		if(heroi.posX == mundo.posPortaoX and heroi.posY == mundo.posPortaoY):
			if(item.possuiItem(3)):
				# CUTSCENE FINAL
				print("Você se aproxima do portão!\nA batalha até esse momento não se provou muito fácil.")
				print("A chave, que possui algumas marcas rúnicas se encaixa perfeitamente no cadeado do portão.")
				pausa()
				print("Você gira a chave lentamente, ouvindo barulhos que parecem se aproximar de você!")
				print("Ao remover o cadeado, você se depara, finalmente, com o portão aberto!")
				pausa()
				print("                 VV     VV iii tt                  iii         \n                 VV     VV     tt     oooo  rr rr        aa aa \n                  VV   VV  iii tttt  oo  oo rrr  r iii  aa aaa \n                   VV VV   iii tt    oo  oo rr     iii aa  aaa \n                    VVV    iii  tttt  oooo  rr     iii  aaa aa ")
				pausa()
				print("\n--------------------------------------------------------------------")
				break
			else:
				print("Você não possui a chave!")
		else:
			print("Você não está perto do portão!")

	elif(menu == "status"):
		print("\n\n\n\n")
		heroi.verStatus(item.retornaAumentoAtk(), item.retornaAumentoForca(), item.retornaAumentoArmadura(), mundo.retornaHorarioStatus())
		item.lerInventario()

	elif(menu == "beber poção" or menu == "beber pocao" or menu == "beberpocao" or menu == "beberpoção" or menu == "bbp"):
		if(heroi.beberPocao(0, getDadoPocao(2, 1), getDadoPocao(3, 1))):
			item.alterarQuantidadeItem(1, getDadoPocao(1, 1), 1)
			totemRegen()
			mundo.adicionarTempo(0,2)

	elif(menu == "beber super poção" or menu == "beber super pocao" or menu == "bebersuperpocao" or menu == "bebersuperpoção" or menu == "bbsp"):
		if(heroi.beberPocao(1, getDadoPocao(2, 2), getDadoPocao(3, 2))):
			item.alterarQuantidadeItem(1, getDadoPocao(1, 2), 1)
			totemRegen()
			mundo.adicionarTempo(0,2)
			
	elif(menu == "antidoto" or menu == "antídoto" or menu == "ant"):
		if(item.possuiItem(12)):
			if(item.retornaQuantidadeItem(12) > 0 and heroi.passosRestantesVeneno > 0):
				print("Você tomou um pote de antídoto e parou de sentir os sintomas do veneno!")
				heroi.passosRestantesVeneno = 0
				item.alterarQuantidadeItem(1, item.retornaPosicaoItem(12), 1)
				totemRegen()
				mundo.adicionarTempo(0,2)
			else:
				print("Você não está envenenado!")
		else:
			print("Você não possui nenhum antídoto no inventário")

	elif(menu == "mapa" or menu == "map"):
		print("+------------------+\n|      ~MAPA~      |\n|                  |")
		# Verifica se o herói já passou pelo portão
		if(heroi.achouPortao == 1):
			
			tmp = "|Portão  - X" # Começo da mensagem
			if(mundo.posPortaoX >=10): # Se posição for maior/igual a 10
				tmp = tmp + str(mundo.posPortaoX) # Adiciona o valor inteiro
			else: # Se não for maior/igual a 10
				tmp = tmp + "0" + str(mundo.posPortaoX) # Adiciona valor com 0

			tmp = tmp + " Y" # Adiciona meio da mensagem
			if(mundo.posPortaoY >=10): # Se posição Y for maior/igual a 10
				tmp = tmp + str(mundo.posPortaoY) # Adiciona o valor inteiro
			else: # Se não for maior/igual a 10
				tmp = tmp + "0" + str(mundo.posPortaoY) # Adiciona valor com 0

			tmp = tmp + " |" # Conclui a mensagem
			print(tmp)
				
		else:
			print("|Portão  - X?? Y?? |")

		# Verifica se o herói já passou pela loja
		if(heroi.achouLoja == 1):

			tmp = "|Loja    - X" # Começo da mensagem
			if(mundo.posNPCX >=10): # Se posição for maior/igual a 10
				tmp = tmp + str(mundo.posNPCX) # Adiciona o valor inteiro
			else: # Se não for maior/igual a 10
				tmp = tmp + "0" + str(mundo.posNPCX) # Adiciona valor com 0

			tmp = tmp + " Y" # Adiciona meio da mensagem
			if(mundo.posNPCY >=10): # Se posição Y for maior/igual a 10
				tmp = tmp + str(mundo.posNPCY) # Adiciona o valor inteiro
			else: # Se não for maior/igual a 10
				tmp = tmp + "0" + str(mundo.posNPCY) # Adiciona valor com 0

			tmp = tmp + " |" # Conclui a mensagem
			print(tmp)
		else:
			print("|Loja    - X?? Y?? |")

		# Verifica se o herói já passou pela pousada
		if(heroi.achouPousada == 1):

			tmp = "|Pousada - X" # Começo da mensagem
			if(mundo.posPouX >=10): # Se posição for maior/igual a 10
				tmp = tmp + str(mundo.posPouX) # Adiciona o valor inteiro
			else: # Se não for maior/igual a 10
				tmp = tmp + "0" + str(mundo.posPouX) # Adiciona valor com 0

			tmp = tmp + " Y" # Adiciona meio da mensagem
			if(mundo.posPouY >=10): # Se posição Y for maior/igual a 10
				tmp = tmp + str(mundo.posPouY) # Adiciona o valor inteiro
			else: # Se não for maior/igual a 10
				tmp = tmp + "0" + str(mundo.posPouY) # Adiciona valor com 0

			tmp = tmp + " |" # Conclui a mensagem
			print(tmp)
		else:
			print("|Pousada - X?? Y?? |")

		# Verifica se o herói já passou pela chave E não pegou
		if(heroi.achouChave == 1 and item.possuiItem(3) == 0):
			tmp = "|Chave   - X" # Começo da mensagem
			if(mundo.posChaveX >=10): # Se posição for maior/igual a 10
				tmp = tmp + str(mundo.posChaveX) # Adiciona o valor inteiro
			else: # Se não for maior/igual a 10
				tmp = tmp + "0" + str(mundo.posChaveX) # Adiciona valor com 0

			tmp = tmp + " Y" # Adiciona meio da mensagem
			if(mundo.posChaveY >=10): # Se posição Y for maior/igual a 10
				tmp = tmp + str(mundo.posChaveY) # Adiciona o valor inteiro
			else: # Se não for maior/igual a 10
				tmp = tmp + "0" + str(mundo.posChaveY) # Adiciona valor com 0

			tmp = tmp + " |" # Conclui a mensagem
			print(tmp)
		elif(item.possuiItem(3)):
			print("|Chave   - X** Y** |")
		else:
			print("|Chave   - X?? Y?? |")

		# Imprime o final do mapa
		print("|                  |\n+------------------+")
		mundo.adicionarTempo(0,5)

	elif(menu == "olhar"):
		
		# Seta o alcance do olhar
		alcance = 2
		if(item.possuiItem(10)):
			alcance = 5
		
		# Verifica se há algo para o norte
		if(not item.possuiItem(10)):
			tmpMsg = "Olhou para o norte e "
		else:
			tmpMsg = "Olhou para o norte com o binóculos e "
		if((heroi.posY-1 == mundo.posChaveY or heroi.posY-alcance == mundo.posChaveY) and heroi.posX == mundo.posChaveX):
			tmpMsg = tmpMsg + "viu a chave no chão"
		elif((heroi.posY-1 == mundo.posPortaoY or heroi.posY-alcance == mundo.posChaveY) and heroi.posX == mundo.posPortaoX):
			tmpMsg = tmpMsg + "viu o portão"
		elif((heroi.posY-1 == mundo.posFadY or heroi.posY-alcance == mundo.posFadY) and heroi.posX == mundo.posFadX):
			tmpMsg = tmpMsg + "viu uma fada flutuando"
		elif((heroi.posY-1 == mundo.posNPCY or heroi.posY-alcance == mundo.posNPCY) and heroi.posX == mundo.posNPCX):
			tmpMsg = tmpMsg + "viu a loja"
		elif((heroi.posY-1 == mundo.posPouY or heroi.posY-alcance == mundo.posPouY) and heroi.posX == mundo.posPouX):
			tmpMsg = tmpMsg + "viu a pousada"
		else:
			tmpMsg = tmpMsg + "não viu nada"

		# Verifica se há algo para o sul
		if(not item.possuiItem(10)):
			tmpMsg = tmpMsg + ",\nOlhou para o sul   e "
		else:
			tmpMsg = tmpMsg + ",\nOlhou para o sul   com o binóculos e "
		if((heroi.posY+1 == mundo.posChaveY or heroi.posY+alcance == mundo.posChaveY) and heroi.posX == mundo.posChaveX):
			tmpMsg = tmpMsg + "viu a chave no chão"
		elif((heroi.posY+1 == mundo.posPortaoY or heroi.posY+alcance == mundo.posPortaoY) and heroi.posX == mundo.posPortaoX):
			tmpMsg = tmpMsg + "viu o portão"
		elif((heroi.posY+1 == mundo.posFadY or heroi.posY+alcance == mundo.posFadY) and heroi.posX == mundo.posFadX):
			tmpMsg = tmpMsg + "viu uma fada flutuando"
		elif((heroi.posY+1 == mundo.posNPCY or heroi.posY+alcance == mundo.posNPCY) and heroi.posX == mundo.posNPCX):
			tmpMsg = tmpMsg + "viu a loja"
		elif((heroi.posY+1 == mundo.posPouY or heroi.posY+alcance == mundo.posPouY) and heroi.posX == mundo.posPouX):
			tmpMsg = tmpMsg + "viu a pousada"
		else:
			tmpMsg = tmpMsg + "não viu nada"

		# Verifica se há algo para o oeste
		if(not item.possuiItem(10)):
			tmpMsg = tmpMsg + ",\nOlhou para o oeste e "
		else:
			tmpMsg = tmpMsg + ",\nOlhou para o oeste com o binóculos e "
		if((heroi.posX-1 == mundo.posChaveX or heroi.posX-alcance == mundo.posChaveX) and heroi.posY == mundo.posChaveY):
			tmpMsg = tmpMsg + "viu a chave no chão"
		elif((heroi.posX-1 == mundo.posPortaoX or heroi.posX-alcance == mundo.posPortaoX) and heroi.posY == mundo.posPortaoY):
			tmpMsg = tmpMsg + "viu o portão"
		elif((heroi.posX-1 == mundo.posFadX or heroi.posX-alcance == mundo.posFadX) and heroi.posY == mundo.posFadY):
			tmpMsg = tmpMsg + "viu uma fada flutuando"
		elif((heroi.posX-1 == mundo.posNPCX or heroi.posX-alcance == mundo.posNPCX) and heroi.posY == mundo.posNPCY):
			tmpMsg = tmpMsg + "viu a loja"
		elif((heroi.posX-1 == mundo.posPouX or heroi.posX-alcance == mundo.posPouX) and heroi.posY == mundo.posPouY):
			tmpMsg = tmpMsg + "viu a pousada"
		else:
			tmpMsg = tmpMsg + "não viu nada"

		# Verifica se há algo para o leste
		if(not item.possuiItem(10)):
			tmpMsg = tmpMsg + ",\nOlhou para o leste e "
		else:
			tmpMsg = tmpMsg + ",\nOlhou para o leste com o binóculos e "
		if((heroi.posX+1 == mundo.posChaveX or heroi.posX+alcance == mundo.posChaveX) and heroi.posY == mundo.posChaveY):
			tmpMsg = tmpMsg + "viu a chave no chão"
		elif((heroi.posX+1 == mundo.posPortaoX or heroi.posX+alcance == mundo.posPortaoX) and heroi.posY == mundo.posPortaoY):
			tmpMsg = tmpMsg + "viu o portão"
		elif((heroi.posX+1 == mundo.posFadX or heroi.posX+alcance == mundo.posFadX) and heroi.posY == mundo.posFadY):
			tmpMsg = tmpMsg + "viu uma fada flutuando"
		elif((heroi.posX+1 == mundo.posNPCX or heroi.posX+alcance == mundo.posNPCX) and heroi.posY == mundo.posNPCY):
			tmpMsg = tmpMsg + "viu a loja"
		elif((heroi.posX+1 == mundo.posPouX or heroi.posX+alcance == mundo.posPouX) and heroi.posY == mundo.posPouY):
			tmpMsg = tmpMsg + "viu a pousada"
		else:
			tmpMsg = tmpMsg + "não viu nada"

		print(tmpMsg + ".")
		mundo.adicionarTempo(0,10)

	elif(menu == "largar"):
		itemRemovido = 5
		while(itemRemovido != 0):
			try:
				print("Digite o nº do item a remover:")
				item.lerInventario()
				print("0 - Cancelar")
				itemRemovido = int(input("largar> "))
				if(itemRemovido != 0):
					if(item.inventario[itemRemovido-1].nome == "Chave"):
						print("Esse item é importante demais para ser jogado fora!\nTente outro!\n")
					elif(item.inventario[itemRemovido-1].nome == "VAZIO"):
						print("Não há nenhum item nesse espaço!\nTente outro!\n")
					else:
						item.setarItem(itemRemovido-1, 0, 0)
						print("Item largado!")
						itemRemovido = 0
			except ValueError:
				print("Digite apenas NÚMEROS no campo!\n")

	elif(menu == "repelente" or menu == "repel" or menu == "rep"):
		tmpPos = item.retornaPosicaoItem(8)
		if(tmpPos != -1):
			if(heroi.passosRestantesRepel == 0):
				heroi.passosRestantesRepel = item.inventario[tmpPos].numeroPassos
				item.alterarQuantidadeItem(1, tmpPos, 1)
				print("Você passou repelente, ninguém parece estar seguindo você")
				mundo.adicionarTempo(0,5)
			else:
				print("O efeito de um repelente anterior ainda está em efeito")
		else:
			print("Você não possui nenhum repelente!")

	elif(menu == "sair"):
		tmp = 5
		while(tmp != 0):
			try:
				print("/****/\nVocê deseja salvar o jogo antes de sair?\n1 - Sim\n2 - Não\n0 - Cancelar saída")
				tmp = int(input("> "))
				if(tmp == 1):
					heroi.salvarJogo(mundo.posChaveX, mundo.posChaveY, mundo.posPortaoX, mundo.posPortaoY, mundo.posNPCX, mundo.posNPCY, mundo.posMNX, mundo.posMNY, mundo.posFadX, mundo.posFadY, mundo.posPouX, mundo.posPouY, item.inventario[0].itemID, item.inventario[0].quantidade, item.inventario[1].itemID, item.inventario[1].quantidade, item.inventario[2].itemID, item.inventario[2].quantidade, item.inventario[3].itemID, item.inventario[3].quantidade)
					print("Saindo...")
				elif(tmp == 2):
					print("Saindo sem salvar...")
				else:
					print("Cancelando...\n/****/\n")
					menu = ""
				tmp = 0
			except ValueError:
				print("Digite apenas NÚMEROS no campo!\n")
				
	elif(menu == "ajuda" or menu == "help"):
		print("\n/****/\n\nComandos básicos:\ngo + DIREÇÃO - andar em uma direção\nmapa - exibe um mapa de lugares que já passou\npegar - pega um objeto do chão\nstatus - exibe um resumo do herói\n\n/****/")
	
	#-----------------------------------
	#	   Comandos para testes durante o BETA
	#			   REMOVER DA VERSÃO FINAL!!!
	#-----------------------------------
			
	elif(menu == "inv vazio"):
		print(item.retornaPrimeiroSlotVazio())

	elif(menu == "adicionarExp"):
		novoValor = input("Digite um valor para adicionar:\nadicionarExp> ")
		heroi.exp = heroi.exp + int(novoValor)
		heroi.subirNivel()

	elif(menu == "adicionarGold"):
		novoValor = input("Digite um valor para adicionar:\ndinheiro> ")
		heroi.gold = heroi.gold + int(novoValor)

	elif(menu == "dano"):
		valor = int(input("Digite um valor para receber de dano:\ndano> "))
		heroi.HP = heroi.HP - valor

	elif(menu == "getEspada"):
		item.setarItem(item.retornaPrimeiroSlotVazio(), 4, 1)
		print("Você conseguiu uma espada!")

	elif(menu == "getManopla"):
		item.setarItem(item.retornaPrimeiroSlotVazio(), 5, 1)
		print("Você conseguiu uma manopla!")

	elif(menu == "getEspadaForjada"):
		item.setarItem(item.retornaPrimeiroSlotVazio(), 6, 1)
		print("Você conseguiu uma Espada Forjada!")

	elif(menu == "getManoplaRevestida"):
		item.setarItem(item.retornaPrimeiroSlotVazio(), 7, 1)
		print("Você conseguiu uma manopla revestida!")

	elif(menu == "getRepelente"):
		item.setarItem(item.retornaPrimeiroSlotVazio(), 8, 1)
		print("Você conseguiu um repelente!")
		
	elif(menu == "getEscudo"):
		item.setarItem(item.retornaPrimeiroSlotVazio(), 9, 1)
		print("Você conseguiu um escudo!")
		
	elif(menu == "getBinoculos"):
		item.setarItem(item.retornaPrimeiroSlotVazio(), 10, 1)
		print("Você conseguiu um binóculos!")
		
	elif(menu == "getAntidoto"):
		item.setarItem(item.retornaPrimeiroSlotVazio(), 12, 1)
		print("Você conseguiu um antídoto!")
		
	elif(menu == "getTotemI"):
		item.setarItem(item.retornaPrimeiroSlotVazio(), 13, 1)
		print("Você conseguiu um totem da imortalidade!")
		
	elif(menu == "getTotemR"):
		item.setarItem(item.retornaPrimeiroSlotVazio(), 14, 1)
		print("Você conseguiu um totem da regeneração!")
		
	elif(menu == "cof cof"):
		heroi.passosRestantesVeneno = 50
		print("Veneno por +-50 passos")

	elif(menu == "batalha"):
		batalha()
		
	elif(menu == "diz ai seu bosta"):
		mundo.imprimirPosicoes()

	elif(menu == "cade o mercado"):
		print(mundo.recebeFala(0,0,"Misterioso","Perdidos no planeta"))
		mundo.moverNPC(1)

	elif(menu == "cade a chave"):
		print("A chave? Está a" + mundo.returnDirecaoChave(heroi.posX,heroi.posY))
	#------------------------------------
	#       Fim dos comandos de teste
	#------------------------------------

	else:
		print("Não sei o que '" + menu + "' significa!")
		
	if(heroi.HP < 1 and item.possuiItem(13) == 1):
		print("~~~TOTEM DA IMORTALIDADE~~~\nO totem da imortalidade salvou a sua vida!\nEle quebrou nas suas mãos!\n\n")
		
		# Consome o TOTEM
		item.alterarQuantidadeItem(1,item.retornaPosicaoItem(13),1)
		
		# Cura o veneno e recupera a vida até a metade
		heroi.passosRestantesVeneno = 0
		heroi.HP = int(heroi.maxHP / 2)

if(heroi.HP < 1):
	print(heroi.nome + " morreu!")
