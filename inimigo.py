'''
Adventure Game
Copyright (c) 2015-2021, William Alcântara Costa
All rights reserved.
- This code is licensed under the BSD 3-Clause License - check it on LICENSE.MD

---

Adventure Game (Jogo de aventura)
Copyright (c) 2015-2021, William Alcântara Costa
Todos os direitos reservados.
- Este código está licensiado sob a Licença de três cláusulas BSD - leia mais em LICENSE.MD

'''

import random

nome = "inimigobasico"
HP = 50
maxHP = 50
armadura = 2 # reduz um pouco o dano causado
atkMin = 10
atkMax = 13
velocidade = 5 # aumenta as chances de esquivar de um ataque
forca = 3 # aumenta as chances de criticos
expGanhaMin = 0 # mínimo de experiência que o jogador ganhará
expGanhaMax = 0 # máximo de experiência que o jogador ganhará
goldGanhoMin = 0 # mínimo de dinheiro que o jogador ganhará
goldGanhoMax = 0 # máximo de dinheiro que o jogador ganhará
isDanoCrit = 0 # Se o dano recebido for crítico
chanceVeneno = 0 # Chance de envenenar o jogador

def setarInimigo(nivelJogador, hora):
	global nome, HP, maxHP, armadura, atkMin, atkMax, velocidade, forca, expGanhaMin, expGanhaMax, goldGanhoMin, goldGanhoMax, chanceVeneno
	
	inimigo = random.randint(1, 5)
	if(nivelJogador < 4 and (hora >= 6 and hora < 18)): # Inimigos fracos ao dia
		if(inimigo == 1):
			nome = "Esqueleto"
			HP = 35
			maxHP = 35
			armadura = 1
			atkMin = 5
			atkMax = 8
			velocidade = 3
			forca = 1
			expGanhaMin = 5
			expGanhaMax = 8
			goldGanhoMin = 3
			goldGanhoMax = 5
			chanceVeneno = 0
		elif(inimigo == 2):
			nome = "Demon"
			HP = 40
			maxHP = 40
			armadura = 2
			atkMin = 3
			atkMax = 6
			velocidade = 2
			forca = 2
			expGanhaMin = 9
			expGanhaMax = 13
			goldGanhoMin = 2
			goldGanhoMax = 4
			chanceVeneno = 0
		elif(inimigo == 3):
			nome = "Troll"
			HP = 28
			maxHP = 28
			armadura = 2
			atkMin = 3
			atkMax = 6
			velocidade = 6
			forca = 2
			expGanhaMin = 9
			expGanhaMax = 12
			goldGanhoMin = 2
			goldGanhoMax = 5
			chanceVeneno = 0
		elif(inimigo == 4):
			nome = "Zumbi"
			HP = 52
			maxHP = 52
			armadura = 1
			atkMin = 5
			atkMax = 9
			velocidade = 2
			forca = 1
			expGanhaMin = 12
			expGanhaMax = 16
			goldGanhoMin = 5
			goldGanhoMax = 9
			chanceVeneno = 5
		elif(inimigo == 5):
			nome = "Aranha gigante"
			HP = 40
			maxHP = 40
			armadura = 1
			atkMin = 1
			atkMax = 3
			velocidade = 4
			forca = 1
			expGanhaMin = 7
			expGanhaMax = 11
			goldGanhoMin = 3
			goldGanhoMax = 7
			chanceVeneno = 4
	elif(nivelJogador < 4 and (hora < 6 or hora >= 18)): # Inimigos fracos a noite
		if(inimigo == 1):
			nome = "Esqueleto"
			HP = 40
			maxHP = 40
			armadura = 1
			atkMin = 6
			atkMax = 10
			velocidade = 4
			forca = 1
			expGanhaMin = 6
			expGanhaMax = 10
			goldGanhoMin = 4
			goldGanhoMax = 6
			chanceVeneno = 0
		elif(inimigo == 2):
			nome = "Demon"
			HP = 46
			maxHP = 46
			armadura = 2
			atkMin = 4
			atkMax = 7
			velocidade = 2
			forca = 3
			expGanhaMin = 11
			expGanhaMax = 14
			goldGanhoMin = 3
			goldGanhoMax = 6
			chanceVeneno = 0
		elif(inimigo == 3):
			nome = "Troll"
			HP = 33
			maxHP = 33
			armadura = 2
			atkMin = 4
			atkMax = 8
			velocidade = 7
			forca = 3
			expGanhaMin = 11
			expGanhaMax = 13
			goldGanhoMin = 2
			goldGanhoMax = 5
			chanceVeneno = 0
		elif(inimigo == 4):
			nome = "Zumbi"
			HP = 60
			maxHP = 60
			armadura = 1
			atkMin = 5
			atkMax = 11
			velocidade = 2
			forca = 1
			expGanhaMin = 14
			expGanhaMax = 18
			goldGanhoMin = 7
			goldGanhoMax = 12
			chanceVeneno = 4
		elif(inimigo == 5):
			nome = "Aranha gigante"
			HP = 46
			maxHP = 46
			armadura = 1
			atkMin = 1
			atkMax = 3
			velocidade = 5
			forca = 1
			expGanhaMin = 7
			expGanhaMax = 11
			goldGanhoMin = 3
			goldGanhoMax = 7
			chanceVeneno = 3
	elif(nivelJogador >= 4 and (hora >= 6 and hora < 18)): # Inimigos fortes ao dia
		if(inimigo == 1):
			nome = "Esqueleto com armadura"
			HP = 60
			maxHP = 60
			armadura = 7
			atkMin = 8
			atkMax = 11
			velocidade = 4
			forca = 6
			expGanhaMin = 11
			expGanhaMax = 15
			goldGanhoMin = 7
			goldGanhoMax = 10
			chanceVeneno = 0
		elif(inimigo == 2):
			nome = "Devil"
			HP = 62
			maxHP = 62
			armadura = 4
			atkMin = 7
			atkMax = 10
			velocidade = 3
			forca = 5
			expGanhaMin = 13
			expGanhaMax = 17
			goldGanhoMin = 9
			goldGanhoMax = 15
			chanceVeneno = 0
		elif(inimigo == 3):
			nome = "Orc"
			HP = 80
			maxHP = 80
			armadura = 8
			atkMin = 10
			atkMax = 15
			velocidade = 6
			forca = 7
			expGanhaMin = 22
			expGanhaMax = 25
			goldGanhoMin = 11
			goldGanhoMax = 16
			chanceVeneno = 0
		elif(inimigo == 4):
			nome = "Ex-Herói Zumbi"
			HP = 85
			maxHP = 85
			armadura = 3
			atkMin = 12
			atkMax = 17
			velocidade = 4
			forca = 3
			expGanhaMin = 25
			expGanhaMax = 29
			goldGanhoMin = 20
			goldGanhoMax = 26
			chanceVeneno = 4
		elif(inimigo == 5):
			nome = "Cobra gigante"
			HP = 70
			maxHP = 70
			armadura = 2
			atkMin = 8
			atkMax = 13
			velocidade = 4
			forca = 1
			expGanhaMin = 15
			expGanhaMax = 20
			goldGanhoMin = 10
			goldGanhoMax = 14
			chanceVeneno = 3
	else: # Inimigos fortes a noite
		if(inimigo == 1):
			nome = "Esqueleto com armadura"
			HP = 69
			maxHP = 69
			armadura = 7
			atkMin = 10
			atkMax = 14
			velocidade = 4
			forca = 7
			expGanhaMin = 13
			expGanhaMax = 17
			goldGanhoMin = 9
			goldGanhoMax = 14
			chanceVeneno = 0
		elif(inimigo == 2):
			nome = "Devil"
			HP = 72
			maxHP = 72
			armadura = 5
			atkMin = 7
			atkMax = 10
			velocidade = 3
			forca = 6
			expGanhaMin = 15
			expGanhaMax = 19
			goldGanhoMin = 9
			goldGanhoMax = 15
			chanceVeneno = 0
		elif(inimigo == 3):
			nome = "Orc"
			HP = 92
			maxHP = 92
			armadura = 5
			atkMin = 11
			atkMax = 16
			velocidade = 6
			forca = 7
			expGanhaMin = 26
			expGanhaMax = 29
			goldGanhoMin = 17
			goldGanhoMax = 21
			chanceVeneno = 0
		elif(inimigo == 4):
			nome = "Ex-Herói Zumbi"
			HP = 98
			maxHP = 98
			armadura = 3
			atkMin = 13
			atkMax = 18
			velocidade = 4
			forca = 3
			expGanhaMin = 28
			expGanhaMax = 31
			goldGanhoMin = 22
			goldGanhoMax = 27
			chanceVeneno = 4
		elif(inimigo == 5):
			nome = "Cobra gigante"
			HP = 80
			maxHP = 80
			armadura = 2
			atkMin = 8
			atkMax = 13
			velocidade = 5
			forca = 1
			expGanhaMin = 15
			expGanhaMax = 20
			goldGanhoMin = 10
			goldGanhoMax = 14
			chanceVeneno = 3
		
def atacar():
	global atkMin, atkMax, forca, isDanoCrit
	isCritico = random.randint(1, 10-forca) # testa a chance de um ataque critico
	ataque = random.randint(atkMin, atkMax) # gera o valor de um ataque
	if(isCritico != 1): # se o ataque NÃO for critico
		isDanoCrit = 0
		return ataque
	else: # se for crítico
		isDanoCrit = 1
		return ataque * 2 # duplica o ataque

def receberDano(quantidadeDano, recebeuCritico):
	global HP, armadura, velocidade
	chanceEsquiva = random.randint(1, 15-velocidade)
	if(chanceEsquiva != 1): # se NÃO esquivar o ataque
		HP = HP - (quantidadeDano - (int(armadura * 0.65)))
		if(recebeuCritico == 1):
			print("Você causou " + str(quantidadeDano - (int(armadura * 0.65))) + " de dano CRÍTICO!")		
		else:
			print("Você causou " + str(quantidadeDano - (int(armadura * 0.65))) + " de dano!")
	else:
		print("O inimigo esquivou seu golpe!")

def retornarExpGanha():
	global expGanhaMin, expGanhaMax
	return random.randint(expGanhaMin, expGanhaMax)

def retornarGoldGanho():
	global goldGanhoMin, goldGanhoMax
	return random.randint(goldGanhoMin, goldGanhoMax)

def chanceDroparPocao():
	return random.randint(1, 5)

def isVeneno():
	global chanceVeneno
	if(chanceVeneno == 0):
		return 0
	else:
		return random.randint(1,chanceVeneno)
