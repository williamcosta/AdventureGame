# AdventureGame
Um jogo de aventura em linha de comando feito em Python;

## História
Não há uma história elaborada, você está em um campo fechado por todos os lados, você ve um loja, uma pousada, alguém escondido em sombras, além de um portão com um cadeado.
Você precisa encontrar a chave e o portão para poder fugir.

## Comandos básicos

**salvarJogo** - Salva o jogo em um arquivo;

**abrirJogo** - Carrega um jogo antigo;

**go** + *N* ou *S* ou *L* ou *O* - Caminha norte, sul, leste ou oeste;

**pegar** - pegar algum objeto no chão;

**abrir** - abrir o portão;

**beber poção** ou **beberpocao** ou **bbp** - bebe uma poção para recuperar 20 de HP;

**beber super poção** ou **bebersuperpocao** ou **bbsp** - bebe uma super poção para recuperar 50 de HP;

**olhar** - olha em volta, informando o que há;

**status** - exibe estado atual do herói;

**falar** - fala com algum NPC próximo, a loja, a pousada ou o mercado negro;

## Requisitos
Python 3.5+
