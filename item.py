'''
Adventure Game
Copyright (c) 2015-2021, William Alcântara Costa
All rights reserved.
- This code is licensed under the BSD 3-Clause License - check it on LICENSE.MD

---

Adventure Game (Jogo de aventura)
Copyright (c) 2015-2021, William Alcântara Costa
Todos os direitos reservados.
- Este código está licensiado sob a Licença de três cláusulas BSD - leia mais em LICENSE.MD

'''

import random # só pra ter certeza

class SubItem():
    itemID = 0
    nome = "VAZIO"
    descricao = ""
    quantidade = 0
    curaHP = 0
    aumentoAtk = 0
    aumentoArmadura = 0
    aumentoForca = 0
    numeroPassos = 0

inventario = (SubItem(), SubItem(), SubItem(), SubItem())

def setarItem(posicao, tipo, quant):
    if(tipo == 1): # Poção
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Poção"
        inventario[posicao].descricao = "Curam 25HP do herói"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 25
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 0

    elif(tipo == 2): # Super-poção
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Super Poção"
        inventario[posicao].descricao = "Curam 50HP do herói"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 50
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 0

    elif(tipo == 3): # Chave
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Chave"
        inventario[posicao].descricao = "Usada para abrir o portão"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        
    elif(tipo == 4): # Espada
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Espada"
        inventario[posicao].descricao = "Aumenta o ataque em 5 pontos"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 5
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 0

    elif(tipo == 5): # Manopla
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Manopla"
        inventario[posicao].descricao = "Aumenta a força em 1 ponto"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 1

    elif(tipo == 6): # Espada forjada
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Espada Forjada"
        inventario[posicao].descricao = "Aumenta o ataque em 12 pontos"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 12
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 0

    elif(tipo == 7): # Manopla Revestida
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Manopla revestida"
        inventario[posicao].descricao = "Aumenta a força em 2 pontos"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 2
        inventario[posicao].numeroPassos = 0

    elif(tipo == 8): # Repelente
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Repelente"
        inventario[posicao].descricao = "Evita encontro com inimigos por 15 passos"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 15

    elif(tipo == 9): # Escudo
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Escudo"
        inventario[posicao].descricao = "Aumenta armadura em 2 pontos"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 2
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 0

    elif(tipo == 10): # Binóculos
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Binóculos"
        inventario[posicao].descricao = "Aumenta o alcance da visão em 3 pontos"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 0
        
    elif(tipo == 11): # Porrete
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Porrete"
        inventario[posicao].descricao = "Aumenta o ataque em 2 pontos"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 2
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 0
        
    elif(tipo == 12): # Antídoto
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Antídoto"
        inventario[posicao].descricao = "Remove os efeitos de veneno do corpo"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 0
        
    elif(tipo == 13): # Totem da imortalidade
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Totem da imortalidade"
        inventario[posicao].descricao = "Impede o usuário de morrer"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 0
        
    elif(tipo == 14): # Totem da regeneração
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "Totem da regeneração"
        inventario[posicao].descricao = "Cura 1HP a cada ação"
        inventario[posicao].quantidade = quant
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0
        inventario[posicao].aumentoForca = 0
        inventario[posicao].numeroPassos = 0

    else: # Item inválido
        inventario[posicao].itemID = tipo
        inventario[posicao].nome = "VAZIO"
        inventario[posicao].descricao = ""
        inventario[posicao].quantidade = 0
        inventario[posicao].curaHP = 0
        inventario[posicao].aumentoAtk = 0
        inventario[posicao].aumentoArmadura = 0

def alterarQuantidadeItem(isConsumir, pos, quant):
    if(isConsumir): # Se estiver consumindo o item
        inventario[pos].quantidade = inventario[pos].quantidade - quant
        if(inventario[pos].quantidade == 0): # Verifica se acabou o item
            setarItem(pos, 0, 0) # Limpa o slot do inventário
    else: # Se não consumir o item
        inventario[pos].quantidade = inventario[pos].quantidade + quant

def retornaPosicaoItem(idItem):
    valorRetorno = -1
    for x in range(0,4):
        if(inventario[x].itemID == idItem):
           valorRetorno = int(x)
           break
    return valorRetorno

def possuiItem(idItem):
    valorRetorno = 0
    for x in range(0,4):
           if(inventario[x].itemID == idItem):
                valorRetorno = 1
                break
    return valorRetorno

def retornaPrimeiroSlotVazio():
    valorRetorno = -1
    for x in range(0,4):
        if(inventario[x].nome == "VAZIO"):
            valorRetorno = x
            break
    return valorRetorno
    
def retornaQuantidadeItem(idItem):
    return inventario[retornaPosicaoItem(idItem)].quantidade

def retornaAumentoAtk():
    valorRetorno = 0
    for x in range(0,4):
        valorRetorno+= inventario[x].aumentoAtk
    return valorRetorno

def retornaAumentoForca():
    valorRetorno = 0
    for x in range(0,4):
        valorRetorno+= inventario[x].aumentoForca
    return valorRetorno

def retornaAumentoArmadura():
    valorRetorno = 0
    for x in range(0,4):
        valorRetorno+= inventario[x].aumentoArmadura
    return valorRetorno
    
def lerInventario():
    for x in range(0,4):
        if(inventario[x].nome != "VAZIO"):
            print(str(x+1) + " - " + inventario[x].nome + " (Q: " + str(inventario[x].quantidade) + ") - " + inventario[x].descricao)
        else:
            print(str(x+1) + " - " + inventario[x].nome)
