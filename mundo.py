'''
Adventure Game
Copyright (c) 2015-2021, William Alcântara Costa
All rights reserved.
- This code is licensed under the BSD 3-Clause License - check it on LICENSE.MD

---

Adventure Game (Jogo de aventura)
Copyright (c) 2015-2021, William Alcântara Costa
Todos os direitos reservados.
- Este código está licensiado sob a Licença de três cláusulas BSD - leia mais em LICENSE.MD

'''

import random

# Limites do mapa
limiteX = 10
limiteY = 10

# Tempo decorrido
dias = 1
horas = random.randint(4,15)
minutos = random.randint(0,59)

# Posição da chave
posChaveX = 0
posChaveY = 0

# Posição do portao de saída
posPortaoX = 0
posPortaoY = 0

# Posição da loja e variável de catástrofe
posNPCX = 0
posNPCY = 0

# Posição do mercado Negro
posMNX = 0
posMNY = 0

# Posição da fada
posFadX = 0
posFadY = 0

# Posição da Pousada
posPouX = 0
posPouY = 0

# Item falso no chão?
isItemFalso = 0

def salvarPosicoes(objeto, tipo):
	global posChaveX, posChaveY, posPortaoX, posPortaoY, posNPCX, posNPCY, posMNX, posMNY, posFadX, posFadY, posPouX, posPouY
	if(objeto == 1 and tipo == 1):
		return posChaveX
	elif(objeto == 1 and tipo == 2):
		return posChaveY
	elif(objeto == 2 and tipo == 1):
		return posPortaoX
	elif(objeto == 2 and tipo == 2):
		return posPortaoY
	elif(objeto == 3 and tipo == 1):
		return posNPCX
	elif(objeto == 3 and tipo == 2):
		return posNPCY
	elif(objeto == 4 and tipo == 1):
		return posMNX
	elif(objeto == 4 and tipo == 2):
		return posMNY
	elif(objeto == 5 and tipo == 1):
		return posFadX
	elif(objeto == 5 and tipo == 2):
		return posFadY
	elif(objeto == 6 and tipo == 1):
		return posPouX
	else:
		return posPouY

def abrirPosicoes(chaveX, chaveY, portaoX, portaoY, NPCX, NPCY, MNX, MNY, fadX, fadY, pouX, pouY):
	global posChaveX, posChaveY, posPortaoX, posPortaoY, posNPCX, posNPCY, posMNX, posMNY, posFadX, posFadY, posPouX, posPouY
	posChaveX = chaveX
	posChaveY = chaveY
	posPortaoX = portaoX
	posPortaoY = portaoY
	posNPCX = NPCX
	posNPCY = NPCY
	posMNX = MNX
	posMNY = MNY
	posFadX = fadX
	posFadY = fadY
	posPouX = pouX
	posPouY = pouY

def sortearPosicaoChave():
	global posChaveX, posChaveY
	posChaveX = random.randint(1, limiteX)
	posChaveY = random.randint(1, limiteY)

def sortearPosicaoPortao():
	global posPortaoX, posPortaoY, limiteX, limiteY, posChaveX, posChaveY
	direcao = random.randint(1, 4)
	if(direcao == 1): # norte
		posPortaoX = random.randint(1, limiteX)
		while(posPortaoX == posChaveX):
			posPortaoX = random.randint(1, limiteX)
		posPortaoY = 1
	elif(direcao == 2): # sul
		posPortaoX = random.randint(1, limiteX)
		while(posPortaoX == posChaveX):
			posPortaoX = random.randint(1, limiteX)
		posPortaoY = limiteY
	elif(direcao == 3): # leste
		posPortaoX = limiteX
		posPortaoY = random.randint(1, limiteY)
		while(posPortaoY == posChaveY):
			posPortaoY = random.randint(1, limiteY)
	else: # oeste
		posPortaoX = 1
		posPortaoY = random.randint(1, limiteY)
		while(posPortaoY == posChaveY):
			posPortaoY = random.randint(1, limiteY)

def sortearPosicaoNPC():
	global posNPCX, posNPCY, limiteX, limiteY, posPortaoX, posPortaoY, posMNX, posMNY, posFadX, posFadY, posPouX, posPouY

	# Sortear a posição da loja
	posNPCX = random.randint(1, limiteX)
	while(posNPCX == posPortaoX):
		posNPCX = random.randint(1, limiteX)
	posNPCY = random.randint(1, limiteY)
	while(posNPCY == posPortaoY):
		posNPCY = random.randint(1, limiteY)

	# Sortear a posição da pousada
	posPouX = random.randint(1, limiteX)
	while(posPouX == posPortaoX or posPouX == posNPCX):
		posPouX = random.randint(1, limiteX)
	posPouY = random.randint(1, limiteY)
	while(posPouY == posPortaoY or posPouY == posNPCY):
		posPouY = random.randint(1, limiteY)

	# Sortear a posição do "Vendedor misterioso"
	posMNX = random.randint(1, limiteX)
	while(posMNX == posNPCX):
		posMNX = random.randint(1, limiteX)
	posMNY = random.randint(1, limiteY)
	while(posMNY == posNPCY):
		posMNY = random.randint(1, limiteY)

	# Sortear a posição da Fada
	posFadX = random.randint(1, limiteX)
	posFadY = random.randint(1, limiteY)

def lerPosicaoAtual(X, Y):
	global posChaveX, posChaveY, posPortaoX, posPortaoY, posNPCX, posNPCY, posPouX, posPouY, isItemFalso
	if(X == posPortaoX and Y == posPortaoY):
		mensagem = "portão"
	elif(X == posNPCX and Y == posNPCY):
		mensagem = "loja"
	elif(X == posPouX and Y == posPouY):
		mensagem = "pousada"
	else:
		mensagem = "terra"
	if(X == posMNX and Y == posMNY):
		mensagem = mensagem + " (alguém parece estar escondido nas sombras)"
	
	# Sorteia a chance de haver um item falso no chão
	if(isItemFalso == 1 and (X != posChaveX and Y != posChaveY)):
		mensagem = mensagem + " (há algo no chão)"
		
	if(X == posChaveX and Y == posChaveY):
		mensagem = mensagem + " (há algo no chão)"
	return mensagem

def moverNPC(tipoNPC):
	global posNPCX, posNPCY, posPortaoX, posPortaoY, posMNX, posMNY, posFadX, posFadY
	'''
	NPCs móveis:
	1 - Vendedor misterioso
	2 - Fada
	'''
	if(tipoNPC == 1):
		possuiIgual = 0 # variável para impedir que ambas as coordenadas sejam iguais
		chanceRoubo = random.randint(1,3)
		
		if(chanceRoubo == 1):
			# Gera uma posição X próxima da loja
			if(posNPCX > 1 and posNPCX < 10):
				posMNX = random.randint(posNPCX-1,posNPCX+1)
			elif(posNPCX == 1):
				posMNX = random.randint(posNPCX,posNPCX+1)
			else:
				posMNX = random.randint(posNPCX-1,posNPC)
			
			# Verifica se a posição X foi gerada junto da loja, assim impedindo que a Y também fique junto
			if(posMNX == posNPCX):
				possuiIgual = 1
				
			# Gera uma posição Y próxima da loja
			if(posNPCY > 1 and posNPCY < 10 and possuiIgual == 0):
				posMNY = random.randint(posNPCY-1,posNPCY+1)
			elif(posNPCY > 1 and posNPCY < 10 and possuiIgual == 1):
				posMNY = random.randint(posNPCY-1,posNPCY+1)
				while(posMNY == posNPCY):
					posMNY = random.randint(posNPCY-1,posNPCY+1)
			elif(posNPCY == 1 and possuiIgual == 0):
				posMNY = random.randint(posNPCY,posNPCY+1)
			elif(posNPCY == 1 and possuiIgual == 1):
				posMNY = posNPCY+1
			elif(posNPCY == 10 and possuiIgual == 0):
				posMNY = random.randint(posNPCY,posNPCY+1)
			else:
				posMNY = posNPCY-1
		else:
			# Gera um valor aleatório, depois verifica se ele está próximo da loja
			posMNX = random.randint(1, limiteX)
			while(posMNX == posNPCX-1 or posMNX == posNPCX or posMNX == posNPCX+1):
				posMNX = random.randint(1, limiteX)
			
			# Gera um valor aleatório, depois verifica se está próximo da loja
			posMNY = random.randint(1, limiteY)
			while(posMNY == posNPCY-1 or posMNY == posNPCY or posMNY == posNPCY+1):
				posMNY = random.randint(1, limiteY)
	else:
		posFadX = random.randint(1, limiteX)
		posFadY = random.randint(1, limiteY)

def recebeFala(isVenda,dinheiro,nome, mensagem):
	msg = "+-----------\n| " + nome + "\n+-----------\n| " + mensagem
	if(isVenda == 1):
		msg = msg + "\n+-----------\n|Seu dinheiro: " + str(dinheiro) + "$"
	return msg + "\n+-----------"

def returnDirecaoChave(posX, posY):
	global posChaveX, posChaveY
	if(posX == posChaveX and posY == posChaveY):
		return "qui"
	elif(posX == posChaveX and posY > posChaveY):
		return " norte"
	elif(posX == posChaveX and posY < posChaveY):
		return " sul"
	elif(posX > posChaveX and posY == posChaveY):
		return " oeste"
	elif(posX > posChaveX and posY > posChaveY):
		return " noroeste"
	elif(posX > posChaveX and posY < posChaveY):
		return " sudoeste"
	elif(posX < posChaveX and posY == posChaveY):
		return " leste"
	elif(posX < posChaveX and posY > posChaveY):
		return " nordeste"
	elif(posX < posChaveX and posY < posChaveY):
		return " sudeste"
	else:
		return " ERROR"

def retornaNumeroQuarto():
	andar = random.randint(1,3)
	quarto = random.randint(0,9)
	return str(andar) + "0" + str(quarto)

def retornaAssaltoPousada():
	global posNPCX, posNPCY, posMNX, posMNY
	resultado = 0
	if(posMNX >= posNPCX-1 and posMNX <= posNPCX+1):
		if(posMNY >= posNPCY-1 and posMNY <= posNPCY+1):
			resultado = 1
	return resultado

def moverChave():
	global posChaveX, limiteX
	posChaveX = limiteX+8
	
def sortearItemFalso():
	global isItemFalso
	tmp = random.randint(1,6)
	if(tmp > 1):
		isItemFalso = 0
	else:
		isItemFalso = tmp
		
# ----------------
#  FUNÇÕES DO TEMPO
# ----------------

def checarMudancaTempo():
	global dias, horas, minutos
	
	if(minutos >= 60):
		horas = horas + 1
		minutos = minutos - 60
	
	if(horas >= 24):
		dias = dias + 1
		horas = horas - 24
		
def adicionarTempo(maisHora, maisMinuto):
	global horas, minutos
	horas = horas + maisHora
	minutos = minutos + maisMinuto
	checarMudancaTempo()
	
def avancarUmDia():
	global dias, horas, minutos
	dias = dias + 1
	horas = 6
	minutos = 0
	
def retornarHorario():
	global horas, minutos
	
	x = ""
	if(horas < 10):
		x = "0" + str(horas)
	else:
		x = str(horas)
	
	if(minutos < 10):
		x = x + ":0" + str(minutos)
	else:
		x = x + ":" + str(minutos)
		
	return x
	
def retornaHorarioStatus():
	global dias, horas, minutos
	
	if(horas < 10 and minutos < 10):
		x = str(dias) + "º dia - 0" + str(horas) + ":0" + str(minutos)
	elif(horas >= 10 and minutos < 10):
		x = str(dias) + "º dia - " + str(horas) + ":0" + str(minutos)
	elif(horas < 10 and minutos >= 10):
		x = str(dias) + "º dia - 0" + str(horas) + ":" + str(minutos)
	else:
		x = str(dias) + "º dia - " + str(horas) + ":" + str(minutos)
		
	return x
	
def abrirHorario(tDias, tHoras, tMinutos):
	global dias, horas, minutos
	dias = tDias
	horas = tHoras
	minutos = tMinutos
	
		
def imprimirPosicoes():
	global posChaveX, posChaveY, posPortaoX, posPortaoY, posNPCX, posNPCY, posPouX, posPouY
	print("Chave: X" + str(posChaveX) + " / Y" + str(posChaveY))
	print("Portão: X" + str(posPortaoX) + " / Y" + str(posPortaoY))
	print("Loja: X" + str(posNPCX) + " / Y" + str(posNPCY))
	print("Mercado Negro: X" + str(posMNX) + " / Y" + str(posMNY))
	print("Fada: X" + str(posFadX) + " / Y" + str(posFadY))
	print("Pousada: X" + str(posPouX) + " / Y" + str(posPouY))
